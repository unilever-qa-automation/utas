package com.sapient.utas.d2;

import java.util.logging.Level;
import java.util.logging.Logger;

public class HelperClass {
	/**
	 * This method is used to kill all the processes running chrome server if
	 * any is running as system processes.
	 *
	 **/
	private static void killChromeService() {
		String serviceName = "chromedriver.exe";
		try {
			if (ProcessKiller.isProcessRunning(serviceName)) {
				ProcessKiller.killProcess(serviceName);
			}
		} catch (Exception ex) {
			Logger.getLogger(HelperClass.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is used to kill all the processes running phantomjs driver if
	 * any is running as system processes.
	 *
	 **/
	private static void killPhantomJS() {
		String serviceName = "phtantomjs.exe";
		try {
			if (ProcessKiller.isProcessRunning(serviceName)) {
				ProcessKiller.killProcess(serviceName);
			}
		} catch (Exception ex) {
			Logger.getLogger(HelperClass.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is used to kill all the processes running internet explorer
	 * server if any is running as system processes.
	 *
	 **/
	private static void killIEService() {
		String serviceName = "IEDriverServer.exe";
		try {
			if (ProcessKiller.isProcessRunning(serviceName)) {
				ProcessKiller.killProcess(serviceName);
			}
		} catch (Exception ex) {
			Logger.getLogger(HelperClass.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is used to kill all running browser server processes if any
	 * is running as system processes.
	 *
	 **/
	public static void killServers() {
		killChromeService();
		killIEService();
		killPhantomJS();
	}
}
