package com.sapient.utas.d2.exceptions;

public class JSONException extends Exception {

	private static final long serialVersionUID = 1L;

	public JSONException() {

	}

	public JSONException(String arg0) {
		super(arg0);
	}

	public JSONException(Throwable arg0) {
		super(arg0);
	}

	public JSONException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public JSONException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
