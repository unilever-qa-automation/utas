package com.sapient.utas.d2.exceptions;

public class D2Exception extends Exception {
	private static final long serialVersionUID = 1L;

	public D2Exception() {
	}

	public D2Exception(String message) {
		super(message);
	}

	public D2Exception(Throwable cause) {
		super(cause);
	}

	public D2Exception(String message, Throwable cause) {
		super(message, cause);
	}

	public D2Exception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
