package com.sapient.utas.d2.helpers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SearchResult {
	private Document doc;
	private String name;
	private double rating;

	public String getName() {
		return name;
	}

	public double getRating() {
		return rating;
	}

	public void setElement(String element) {
		doc = Jsoup.parse(element);
		setName();
		setRating();
	}

	private void setName() {
		name = doc.select("h2.o-text__heading-3").first().text();
	}

	private void setRating() {
		try {
			String text = doc.select("span.bv-off-screen").first().text();
			rating = Double.parseDouble(text.substring(0, text.indexOf(" out")));
		} catch (Exception e) {
			rating = 0;
		}
	}
}
