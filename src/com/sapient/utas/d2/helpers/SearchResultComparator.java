package com.sapient.utas.d2.helpers;

import java.util.Comparator;

public class SearchResultComparator implements Comparator<SearchResult> {

	@Override
	public int compare(SearchResult o1, SearchResult o2) {
		if (o1.getRating() > o2.getRating())
			return 1;
		else if (o1.getRating() < o2.getRating())
			return -1;
		else {
			return o1.getName().compareTo(o2.getName());
		}
	}

}
