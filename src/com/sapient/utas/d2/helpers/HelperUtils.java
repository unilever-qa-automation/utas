package com.sapient.utas.d2.helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HelperUtils {
	public static List<WebElement> loadAllElements(WebDriver driver) throws Exception {
		try {
			WebElement element = new WebDriverWait(driver, 5).until(
					ExpectedConditions.elementToBeClickable(By.cssSelector("div.c-search-listing__loadmore button")));
			if (null != element) {
				element.click();
			}
		} catch (Exception e) {
		}

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement last = driver.findElement(By.xpath(
				"(.//div[@class='c-search-listing__wrap']//li[contains(@class,'c-search-listing__item')])[last()]"));
		js.executeScript("arguments[0].scrollIntoView(true);", last);
		boolean visible = true;
		while (visible) {
			last = driver.findElement(By.xpath(
					"(.//div[@class='c-search-listing__wrap']//li[contains(@class,'c-search-listing__item')])[last()]"));
			js.executeScript("arguments[0].scrollIntoView(true);", last);
			Thread.sleep(2000);
			if (driver.findElement(By.cssSelector(".c-search-listing__loadmore")).getCssValue("display")
					.equals("block"))
				visible = true;
			else
				visible = false;
		}
		return driver.findElements(
				By.xpath("(.//div[@class='c-search-listing__wrap']//li[contains(@class,'c-search-listing__item')])"));
	}
}
