package com.sapient.utas.scripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sapient.utas.suite.Suite;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

public class Utilities {
	Workbook workbook, tcWorkbook, tdWorkbook;
	ArrayList<String> arrObjectRepository = new ArrayList<>();
	ArrayList<String> arrSuiteName = new ArrayList<>();
	Hashtable<String, Hashtable<String, String>> suiteTestDataMap = new Hashtable<>();
	ArrayList<Suite> arrSuite = new ArrayList<>();
	ArrayList<String> arrTSName = new ArrayList<>();
	Sheet worksheet, envWorksheet, tdWorksheet, tsWorksheet, tcWorksheet;
	String sTestSuite, sTestSuiteStatus, sTestScenario, sTestScenarioStatus;
	String sObjectRepository, sTestData, sTDHeader, sDataRun;
	String sStep, sAction, sPage, sObject, sData, sTCMapped, sError;
	int i, j, k, iCol = 1, iRow = 2, iDataloop, iRowCount, iBLColCount, iBLRowCount, iSuiteCount = 0, iBrowserCount = 0,
			iBCLoop;
	int iTDRowCount, iTDCount, iTDColCount, iTCLoop, iTCRowCount, iTSCount;
	int iTotalStepsExecuted;
	WebDriver driver;
	Properties objectRepository;
	By byProperty, byProperty2;
	String sUrl, sBrowser, sBLBrowser;
	Writer output = null;
	String sDateTime, sStoredValue, sURL;
	WebDriverWait wait;
	WebElement element;
	JavascriptExecutor js;
	Reports res = new Reports();
	Keywords key = new Keywords();
	File tcFile;
	long lStartTime, lEndTime, lExecutionTime;
	Hashtable<String, String> globalVariables = new Hashtable<>();
	ArrayList<Hashtable<String, String>> arrGlobalVariablesMap = new ArrayList<>();
	String parentWindowHandle = null;
	String SuiteName;

	public enum keyword {
		ProductFilter, VerifySearchSorting, DoveSearchResultCount, TakePageScreenshot, HasNonEmptyText, FocusElement, VerifyBrokenComponent, VerifyBreadCrumbLevel, CloseWindow, MatchJSON, WaitUntilElementisPresent, SendKey, InsertTag, SelectbyIndex, SelectbyValue, ExecuteJavaScript, DoubleClick, WaitForElementDisappear, VerifyOlapicWidgetID, VerifyHomeStoryText, RichText, Launch, Click, AcceptAlert, Submit, Clear, RightMouseClick, Enter, Select, Hover, ClickAt, Wait, VerifyTitle, VerifyElementEnabled, VerifyElementDisabled, VerifyText, VerifyValue, StoreValue, VerifyStoredValue, SwitchFrame, CreativeTest, VerifyHeight, VerifyWidth, VerifyCreativeAttribute, VerifyTopMargin, VerifyLeftMargin, VerifyHeightBetween, VerifyHeightFrom, VerifyWidthFrom, VerifyWidthBetween, ResizeWindow, InternetExplorerSSL, ConfirmAlert, DeleteCookies, BrowseBack, BrowseForward, RefreshBrowser, VerifyCartCount, VerifyElementNotPresent, VerifyTableCellData, VerifyElementCount, VerifyAttribute, VerifyListItemSelected, VerifyListItem, ClickDynamicObject, VerifyListItemNotPresent, CloseBrowser, Activate, DragAndDrop, SwitchDefaultContent, SwitchParentFrame, EmailValidation, VerifyPartOfText, ClickTableCellLinkByText, SwitchWindow, SwitchParentWindow, VerifyTableCellLinkByText, ClickTableCellLinkByIndex, WaitForPageLoad, EnterHeroMainCopyText, EnterHeroIntroCopyText
	}

	public void loadObjects(String sORName) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will load the Object
			 * Repository
			 */
			objectRepository = new Properties();
			File ORfile = new File("inputs" + File.separator + sORName + ".properties");
			FileInputStream fs = new FileInputStream(ORfile);
			objectRepository.load(fs);

		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in LoadObject function");
			System.out.println(e);
		}
	}

	public void getSuiteName() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will get the Suitename
			 * to be executed from the Controller file
			 */
			File file = new File("inputs" + File.separator + "Controller.xls");
			FileInputStream inputfile = new FileInputStream(file);
			WorkbookSettings ws = new WorkbookSettings();
			ws.setEncoding("utf8");
			workbook = Workbook.getWorkbook(inputfile);
			worksheet = workbook.getSheet("Suite_Controller");
			iRowCount = worksheet.getRows() - 2;
			for (int iLoop = 0; iLoop < iRowCount; iLoop++) {
				sTestSuite = worksheet.getCell(1, iLoop + 2).getContents();
				sTestSuiteStatus = worksheet.getCell(2, iLoop + 2).getContents();
				globalVariables = new Hashtable<>();
				String sGlobalVariableWorkBook = worksheet.getCell(3, iLoop + 2).getContents();
				if (sTestSuiteStatus.equalsIgnoreCase("Yes")) {
					// Store name of all the Test Suite having RUN Status as Yes
					if (!(sGlobalVariableWorkBook.trim() == "" || sGlobalVariableWorkBook == null))
						globalVariables = loadGlobalTestData(sGlobalVariableWorkBook);
					sTestSuite = worksheet.getCell(1, iLoop + 2).getContents();
					arrSuiteName.add(iSuiteCount, sTestSuite);
					arrGlobalVariablesMap.add(globalVariables);
					suiteTestDataMap.put(sTestSuite, globalVariables);
					// System.out.println("Suite Name:: " + sTestSuite);
					iSuiteCount = iSuiteCount + 1;
				}
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetSuiteName function");
			e.printStackTrace();
		}
	}

	private Hashtable<String, String> loadGlobalTestData(String fileName) {
		// TODO Auto-generated method stub
		Hashtable<String, String> globalTestData = new Hashtable<>();
		try {
			fileName = fileName.toUpperCase().endsWith(".XLS") ? fileName : fileName + ".xls";
			File f = new File("inputs" + File.separator + "TestData" + File.separator + fileName);
			if (f.exists()) {
				FileInputStream fisTestDataSheet = new FileInputStream(f);
				WorkbookSettings ws = new WorkbookSettings();
				ws.setEncoding("utf8");
				Workbook globalTestDataWorkBook = Workbook.getWorkbook(fisTestDataSheet, ws);
				Sheet globalTestDataSheet = globalTestDataWorkBook.getSheet(0);
				int lastRowUsed = globalTestDataSheet.getRows();
				for (int i = 1; i < lastRowUsed; i++) {
					String variableName = globalTestDataSheet.getCell(0, i).getContents().trim();
					String variableValue = globalTestDataSheet.getCell(1, i).getContents().trim();
					if (variableName == "")
						break;
					globalTestData.put(variableName, variableValue);
				}
			} else {
				System.out.println("Test Data excel file doesn't exist.");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return globalTestData;
	}

	public void getTSName() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will get the name of
			 * the Test Scenario to be executed from the Controller file
			 */
			tsWorksheet = workbook.getSheet(arrSuiteName.get(i));
			int iTSRowCount = tsWorksheet.getRows() - 2;
			iTSCount = 0;
			for (int iTSLoop = 0; iTSLoop < iTSRowCount; iTSLoop++) {
				sTestScenarioStatus = tsWorksheet.getCell(3, iTSLoop + 2).getContents();
				if (sTestScenarioStatus.trim().equalsIgnoreCase("Yes")) {
					sTestScenario = tsWorksheet.getCell(1, iTSLoop + 2).getContents();
					arrTSName.add(iTSCount, sTestScenario);
					sObjectRepository = tsWorksheet.getCell(2, iTSLoop + 2).getContents();
					arrObjectRepository.add(iTSCount, sObjectRepository);
					iTSCount = iTSCount + 1;
				}
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetTSName function");
			System.out.println(e);
		}
	}

	public void getTestSteps(Reports res, String sTestScenario, WebDriver driver, String deviceName) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will get the Test
			 * Steps for execution
			 */
			if (deviceName.contains("Phone")) {
				tcFile = new File(
						"inputs/AutomatedTestCases/SmallView" + File.separator + arrTSName.get(j) + "_Small.xls");
			} else if (deviceName.contains("Tab")) {
				tcFile = new File(
						"inputs/AutomatedTestCases/MediumView" + File.separator + arrTSName.get(j) + "_Medium.xls");
			} else {
				tcFile = new File(
						"inputs/AutomatedTestCases/LargeView" + File.separator + arrTSName.get(j) + "_Large.xls");
			}
			FileInputStream inputTCfile = new FileInputStream(tcFile);
			WorkbookSettings ws = new WorkbookSettings();
			ws.setEncoding("utf8");
			tcWorkbook = Workbook.getWorkbook(inputTCfile, ws);
			tcWorksheet = tcWorkbook.getSheet("TestScript");
			tdWorksheet = tcWorkbook.getSheet("TestData");
			iTDRowCount = tdWorksheet.getRows() - 2;
			iTDColCount = tdWorksheet.getColumns() - 1;
			iTCRowCount = tcWorksheet.getRows() - 2;
			res.createReport(arrTSName.get(j), deviceName);
			res.createTableHeader(arrTSName.get(j));
			lStartTime = System.currentTimeMillis();
			// System.out.println("Test Data Row Count:: " + iTDRowCount);
			for (int iDataloop = 0; iDataloop < iTDRowCount; iDataloop++) {
				sDataRun = tdWorksheet.getCell(1, iDataloop + 2).getContents();
				// System.out.println("Run Status:: " + sDataRun);
				if (sDataRun.equalsIgnoreCase("Yes")) {
					for (iTCLoop = 0; iTCLoop < iTCRowCount; iTCLoop++) {
						sStep = tcWorksheet.getCell(1, iTCLoop + 2).getContents();
						// System.out.println("Step:: " + sStep);
						sTCMapped = tcWorksheet.getCell(2, iTCLoop + 2).getContents();
						// System.out.println("TCMapped:: " + sTCMapped);
						sAction = tcWorksheet.getCell(3, iTCLoop + 2).getContents();
						// System.out.println("Action:: " + sAction);
						sPage = tcWorksheet.getCell(4, iTCLoop + 2).getContents();
						// System.out.println("Page:: " + sPage);
						sObject = tcWorksheet.getCell(5, iTCLoop + 2).getContents();
						// System.out.println("Object:: " + sObject);
						sError = tcWorksheet.getCell(6, iTCLoop + 2).getContents();
						// System.out.println("Error:: " + sError);
						sData = tcWorksheet.getCell(7, iTCLoop + 2).getContents();
						if (!(sData.isEmpty())) {
							// System.out.println("Test Data Col Count:: " +
							// iTDColCount);
							for (int iHeadLoop = 0; iHeadLoop < iTDColCount; iHeadLoop++) {
								sTDHeader = tdWorksheet.getCell(1 + iHeadLoop, 1).getContents();
								if (sData.equalsIgnoreCase(sTDHeader)) {
									sData = tdWorksheet.getCell(1 + iHeadLoop, iDataloop + 2).getContents();
									// System.out.println("Test Data:: " +
									// sData);
								}
							}
						}
						// if
						// (!arrAction.get(iTCLoop).equalsIgnoreCase("launch"))
						// {
						if (!sAction.equalsIgnoreCase("launch")
								|| !sAction.equalsIgnoreCase("handleChromeAuthentication")) {
							// System.out.println("Object Name to get locator::
							// "
							// + arrObject.get(iTCLoop));
							// if
							// (!arrObject.get(iTCLoop).equalsIgnoreCase(".."))
							// {
							if (!sObject.equalsIgnoreCase("..") && !sObject.equalsIgnoreCase("")) {
								// getLocator(arrObject.get(iTCLoop));
								getLocator(sObject);
							}

						}
						/*
						 * if (arrAction.get(iTCLoop).equalsIgnoreCase(
						 * "verifyHeightBetween")) {
						 * getSecondLocator(arrPage.get(iTCLoop)); } if
						 * (arrAction.get(iTCLoop).equalsIgnoreCase(
						 * "verifyWidthBetween")) {
						 * getSecondLocator(arrPage.get(iTCLoop)); }
						 */
						iTotalStepsExecuted = iTCLoop;
						// getKeywords(arrAction.get(iTCLoop), driver,
						// deviceName);
						System.out.println(sAction + " - " + sPage + " - " + sObject + " - " + sData);
						getKeywords(sAction, sTestScenario, driver, deviceName);
						if (key.iSummaryFlag == 1) {
							// if (arrOnError.get(iTCLoop)
							if (sError.equalsIgnoreCase("Stop")) {
								iTCLoop = iTCRowCount;
								key.iSummaryFlag = 0;
							}
						}
					}
				}
			}
			lEndTime = System.currentTimeMillis();
			lExecutionTime = ((lEndTime - lStartTime) / 1000);
			res.writeTCEnd("END", "END", "END", "END", "END", "END", "END");
			res.closeReport();
			if (key.iSummaryFlag == 1) {
				if (!(key.iTotalStepsPass == 0)) {
					res.writeSummaryFail((j + 1), arrTSName.get(j), iTCRowCount, iTotalStepsExecuted,
							key.iTotalStepsPass, key.iTotalStepsFail, lExecutionTime);
					key.iTotalStepsPass = 0;
					key.iTotalStepsFail = 0;
				}
			} else {
				if (!(key.iTotalStepsPass == 0)) {
					res.writeSummaryPass((j + 1), arrTSName.get(j), iTCRowCount, iTotalStepsExecuted,
							key.iTotalStepsPass, key.iTotalStepsFail, lExecutionTime);
					key.iTotalStepsPass = 0;
					key.iTotalStepsFail = 0;
				}
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetTestSteps function");
			e.printStackTrace();
			System.out.println();
		}
	}

	// Just Created Dummy method
	private void getKeywords(String sAction2, String sTestScenario2, WebDriver driver2, String deviceName) {
		// TODO Auto-generated method stub

	}

	public void getTestSteps(Reports res, String sTestScenario, UTAS_Session utasSession) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will get the Test
			 * Steps for execution
			 */
			if (utasSession.STR_BROWSER_OR_DEVICE.contains("Phone")) {
				tcFile = new File(
						"inputs/AutomatedTestCases/SmallView" + File.separator + arrTSName.get(j) + "_Small.xls");
			} else if (utasSession.STR_BROWSER_OR_DEVICE.contains("Tab")) {
				tcFile = new File(
						"inputs/AutomatedTestCases/MediumView" + File.separator + arrTSName.get(j) + "_Medium.xls");
			} else {
				tcFile = new File(
						"inputs/AutomatedTestCases/LargeView" + File.separator + arrTSName.get(j) + "_Large.xls");
			}
			FileInputStream inputTCfile = new FileInputStream(tcFile);
			WorkbookSettings ws = new WorkbookSettings();
			ws.setEncoding("utf8");
			tcWorkbook = Workbook.getWorkbook(inputTCfile, ws);
			tcWorksheet = tcWorkbook.getSheet("TestScript");
			tdWorksheet = tcWorkbook.getSheet("TestData");
			iTDRowCount = tdWorksheet.getRows() - 2;
			iTDColCount = tdWorksheet.getColumns() - 1;
			iTCRowCount = tcWorksheet.getRows() - 2;
			res.createReport(arrTSName.get(j), utasSession.STR_BROWSER_OR_DEVICE);
			res.createTableHeader(arrTSName.get(j));
			lStartTime = System.currentTimeMillis();
			// System.out.println("Test Data Row Count:: " + iTDRowCount);
			for (int iDataloop = 0; iDataloop < iTDRowCount; iDataloop++) {
				sDataRun = tdWorksheet.getCell(1, iDataloop + 2).getContents();
				// System.out.println("Run Status:: " + sDataRun);
				if (sDataRun.equalsIgnoreCase("Yes")) {
					for (iTCLoop = 0; iTCLoop < iTCRowCount; iTCLoop++) {

						sStep = tcWorksheet.getCell(1, iTCLoop + 2).getContents();
						sTCMapped = tcWorksheet.getCell(2, iTCLoop + 2).getContents();
						sAction = tcWorksheet.getCell(3, iTCLoop + 2).getContents();
						sPage = tcWorksheet.getCell(4, iTCLoop + 2).getContents();
						if (sPage.toUpperCase().startsWith("^"))
							sPage = suiteTestDataMap.get(SuiteName).containsKey(sPage.replace("^", ""))
									? suiteTestDataMap.get(SuiteName).get(sPage.replace("^", ""))
									: sPage.replace("^", "");

						// sPage =
						// globalVariables.containsKey(sPage.replace("^","")) ?
						// globalVariables.get(sPage.replace("^", "")) :
						// sPage.replace("^", "");
						// System.out.println("Page:: " + sPage);
						sObject = tcWorksheet.getCell(5, iTCLoop + 2).getContents();
						if (sObject.toUpperCase().startsWith("^"))
							sObject = suiteTestDataMap.get(SuiteName).containsKey(sObject.replace("^", ""))
									? suiteTestDataMap.get(SuiteName).get(sObject.replace("^", ""))
									: sObject.replace("^", "");
						// sObject =
						// globalVariables.containsKey(sObject.replace("^",""))
						// ? globalVariables.get(sObject.replace("^", "")) :
						// sObject.replace("^", "");

						// System.out.println("Object:: " + sObject);
						sError = tcWorksheet.getCell(6, iTCLoop + 2).getContents();
						// System.out.println("Error:: " + sError);
						sData = tcWorksheet.getCell(7, iTCLoop + 2).getContents();
						if (sData.toUpperCase().startsWith("^"))
							// sData =
							// globalVariables.containsKey(sData.replace("^",""))
							// ? globalVariables.get(sData.replace("^", "")) :
							// sData.replace("^", "");
							sData = suiteTestDataMap.get(SuiteName).containsKey(sData.replace("^", ""))
									? suiteTestDataMap.get(SuiteName).get(sData.replace("^", ""))
									: sData.replace("^", "");

						if (!(sData.isEmpty())) {
							// System.out.println("Test Data Col Count:: " +
							// iTDColCount);
							for (int iHeadLoop = 0; iHeadLoop < iTDColCount; iHeadLoop++) {
								sTDHeader = tdWorksheet.getCell(1 + iHeadLoop, 1).getContents();
								if (sData.equalsIgnoreCase(sTDHeader)) {
									sData = tdWorksheet.getCell(1 + iHeadLoop, iDataloop + 2).getContents();
									// System.out.println("Test Data:: " +
									// sData);
								}
							}
						}
						// if
						// (!arrAction.get(iTCLoop).equalsIgnoreCase("launch"))
						// {
						if (!sAction.equalsIgnoreCase("launch")) {
							// System.out.println("Object Name to get locator::
							// "
							// + arrObject.get(iTCLoop));
							// if
							// (!arrObject.get(iTCLoop).equalsIgnoreCase(".."))
							// {
							if (!sObject.equalsIgnoreCase("..") && !sObject.equalsIgnoreCase("")) {
								// getLocator(arrObject.get(iTCLoop));
								getLocator(sObject);
							}

						}
						/*
						 * if (arrAction.get(iTCLoop).equalsIgnoreCase(
						 * "verifyHeightBetween")) {
						 * getSecondLocator(arrPage.get(iTCLoop)); } if
						 * (arrAction.get(iTCLoop).equalsIgnoreCase(
						 * "verifyWidthBetween")) {
						 * getSecondLocator(arrPage.get(iTCLoop)); }
						 */
						iTotalStepsExecuted = iTCLoop;
						// getKeywords(arrAction.get(iTCLoop), driver,
						// deviceName);
						System.out
								.println(sTCMapped + " - " + sAction + " - " + sPage + " - " + sObject + " - " + sData);
						// getKeywords(sAction, sTestScenario, driver,
						// deviceName);
						getKeywords(sAction, sTestScenario, utasSession);
						if (key.iSummaryFlag == 1) {
							// if (arrOnError.get(iTCLoop)
							if (sError.equalsIgnoreCase("Stop")) {
								iTCLoop = iTCRowCount;
								// key.iSummaryFlag = 0;
							}
						}
					}
				}
			}
			lEndTime = System.currentTimeMillis();
			lExecutionTime = ((lEndTime - lStartTime) / 1000);
			res.writeTCEnd("END", "END", "END", "END", "END", "END", "END");
			res.closeReport();
			if (key.iSummaryFlag == 1) {
				if (!(key.iTotalStepsPass == 0)) {
					res.writeSummaryFail((j + 1), arrTSName.get(j), iTCRowCount, iTotalStepsExecuted,
							key.iTotalStepsPass, key.iTotalStepsFail, lExecutionTime);
					key.iTotalStepsPass = 0;
					key.iTotalStepsFail = 0;
				}
			} else {
				if (!(key.iTotalStepsPass == 0)) {
					res.writeSummaryPass((j + 1), arrTSName.get(j), iTCRowCount, iTotalStepsExecuted,
							key.iTotalStepsPass, key.iTotalStepsFail, lExecutionTime);
					key.iTotalStepsPass = 0;
					key.iTotalStepsFail = 0;
				}
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetTestSteps function");
			System.out.println(e);
		}
	}

	public void getLocator(String sObject) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will decide the
			 * locator to be used for element identification
			 */
			String sProperty = objectRepository.getProperty(sObject);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			// Find the Element Property to be used for Identifying the Object
			if (sLocator.equalsIgnoreCase("css")) {
				byProperty = By.cssSelector(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("id")) {
				byProperty = By.id(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("linkText")) {
				byProperty = By.linkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("name")) {
				byProperty = By.name(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("partialLinkText")) {
				byProperty = By.partialLinkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("tagName")) {
				byProperty = By.tagName(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("xpath")) {
				byProperty = By.xpath(sLocator_Property);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetLocator function");
			System.out.println(e);
		}
	}

	public By getLocatorObject(String sProperty) {
		By byProperty = null;
		try {

			// String sProperty = objectRepository.getProperty(sObject);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			// Find the Element Property to be used for Identifying the Object
			if (sLocator.equalsIgnoreCase("css")) {
				byProperty = By.cssSelector(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("id")) {
				byProperty = By.id(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("linkText")) {
				byProperty = By.linkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("name")) {
				byProperty = By.name(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("partialLinkText")) {
				byProperty = By.partialLinkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("tagName")) {
				byProperty = By.tagName(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("xpath")) {
				byProperty = By.xpath(sLocator_Property);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetLocator function");
			System.out.println(e);
			byProperty = null;
		}
		return byProperty;
	}

	public void getSecondLocator(String sPage) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will decide the
			 * locator to be used for element identification
			 */
			String sProperty = objectRepository.getProperty(sPage);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			// Find the Element Property to be used for Identifying the Object
			if (sLocator.equalsIgnoreCase("css")) {
				byProperty2 = By.cssSelector(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("id")) {
				byProperty2 = By.id(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("linkText")) {
				byProperty2 = By.linkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("name")) {
				byProperty2 = By.name(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("partialLinkText")) {
				byProperty2 = By.partialLinkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("tagName")) {
				byProperty2 = By.tagName(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("xpath")) {
				byProperty2 = By.xpath(sLocator_Property);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetsecondLocator function");
			System.out.println(e);
		}
	}

	@SuppressWarnings("incomplete-switch")
	// public void getKeywords(String sAction, String sTestScenario, WebDriver
	// driver, String deviceName) {
	public void getKeywords(String sAction, String sTestScenario, UTAS_Session utasSession) {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will call the
			 * corresponding Keyword/Function for execution on the basis of
			 * Action
			 */
			String deviceName = utasSession.STR_BROWSER_OR_DEVICE;
			keyword kAction = keyword.valueOf(sAction);
			switch (kAction) {
			case VerifyBrokenComponent:
				key.verifyBrokenComponent(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);

				break;
			case VerifyBreadCrumbLevel:
				key.verifyBreadCrumbLevel(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case ProductFilter:
				key.productFilter(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case MatchJSON:
				key.matchJSON(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);

				break;
			case WaitUntilElementisPresent:
				key.waitUntilElementisPresent(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject,
						sError, sData, sTestScenario, deviceName);
				break;
			case TakePageScreenshot:
				key.takePageScreenshot(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case CloseWindow:
				key.closeWindow(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case WaitForPageLoad:
				key.WaitForPageLoad(this.driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case DoveSearchResultCount:
				key.doveSearchResultCount(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case VerifySearchSorting:
				key.verifySearchSorting(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case FocusElement:
				key.focusElement(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case DeleteCookies:
				key.deleteCookies(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case HasNonEmptyText:
				key.hasNonEmptyText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case BrowseBack:
				key.browseBack(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case BrowseForward:
				key.browseForward(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case RefreshBrowser:
				key.refresh(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case Launch:
				if (this.driver == null) {
					this.driver = new Augmenter()
							.augment(new RemoteWebDriver(new URL(utasSession.STR_HUB_URL), utasSession.CAPABILITIES));
				}
				key.launch(this.driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);

				break;
			case InternetExplorerSSL:
				key.ieSSL(driver, res, sStep, sTCMapped, sAction, sError, sData, sTestScenario, deviceName);
				break;
			case ConfirmAlert:
				key.confirmAlert(driver, res, sStep, sTCMapped, sAction, sError, sData, sTestScenario, deviceName);
				break;
			case Wait:
				key.wait(driver, res, sStep, sTCMapped, sAction, sError, sData);
				break;
			case Click:
				key.click(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case SendKey:
				key.sendKey(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case AcceptAlert:
				key.acceptAlert(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case Submit:
				key.submit(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyHomeStoryText:
				key.verifyHomeStoryText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case RightMouseClick:
				key.rightMouseClick(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;
			case Enter:
				key.enter(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case EnterHeroMainCopyText:
				key.enterHeroMainCopyText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);

				break;
			case EnterHeroIntroCopyText:
				key.enterHeroIntroCopyText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);

				break;
			case Clear:
				key.clear(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sTestScenario,
						deviceName);

				break;
			case Select:
				key.select(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case Hover:
				key.hover(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sTestScenario,
						deviceName);
				break;
			case ClickAt:
				key.clickAt(driver, byProperty, res, sError, sStep, sTCMapped, sAction, sPage, sObject, sTestScenario,
						deviceName);
				break;
			case SwitchFrame:
				key.switchFrame(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario);
				Thread.sleep(2000);
				break;
			case VerifyTitle:
				key.verifyTitle(driver, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData, sTestScenario,
						deviceName);
				break;
			case VerifyElementEnabled:
				key.verifyElementEnabled(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;
			case VerifyElementDisabled:
				key.verifyElementDisabled(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;
			case VerifyElementNotPresent:
				key.verifyElementNotPresent(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;
			case VerifyText:
				key.verifyText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyPartOfText:
				key.verifyPartOfText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyValue:
				key.verifyValue(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case StoreValue:
				key.storeValue(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;
			case VerifyStoredValue:
				key.verifyStoredValue(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sTestScenario, deviceName);
				break;

			case VerifyCreativeAttribute:
				key.verifyCreativeAttribute(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case VerifyHeight:
				key.verifyHeight(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyWidth:
				key.verifyWidth(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyTopMargin:
				key.verifyTopMargin(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyLeftMargin:
				key.verifyLeftMargin(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case ResizeWindow:
				key.resizeWindow(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyTableCellData:
				key.verifyTableCellData(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case VerifyElementCount:
				key.verifyElementCount(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case VerifyAttribute:
				key.verifyAttribute(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyListItemSelected:
				key.verifyListItemSelected(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case VerifyListItem:
				key.verifyListItem(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyListItemNotPresent:
				key.verifyListItemNotPresent(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			// ClickDynamicObject
			case ClickDynamicObject:
				byProperty = getLocatorObject(sData);
				key.clickDynamicObject(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case CloseBrowser:
				key.closeBrowser(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				utasSession.WD = null;
				this.driver = null;
				break;
			case Activate:
				key.activate(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case DragAndDrop:
				String sProperty = objectRepository.getProperty(sData);
				By byPropertyTo = getLocatorObject(sProperty);
				key.dragAndDrop(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						byPropertyTo, sTestScenario);
				break;
			case SwitchDefaultContent:
				key.switchDefaultContent(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario);
				break;
			case SwitchParentFrame:
				key.switchParentFrame(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario);
				break;
			case EmailValidation:
				key.emailValidation(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case ClickTableCellLinkByText:
				key.clickTableCellLinkByText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case ClickTableCellLinkByIndex:
				key.clickTableCellLinkByIndex(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject,
						sError, sData, sTestScenario, deviceName);
				break;

			case SwitchWindow:
				key.switchWindow(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario);
				break;
			case SwitchParentWindow:
				key.switchParentWindow(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						parentWindowHandle, sTestScenario);
				break;

			case VerifyTableCellLinkByText:
				key.verifyTableCellLinkByText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject,
						sError, sData, sTestScenario, deviceName);
				break;
			case RichText:
				key.richText(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case VerifyOlapicWidgetID:
				key.verifyOlapicWidgetID(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case WaitForElementDisappear:
				key.waitForElementDisappear(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError,
						sData, sTestScenario, deviceName);
				break;
			case DoubleClick:
				key.doubleClick(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case ExecuteJavaScript:
				key.executeJavaScript(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case SelectbyValue:
				key.selectbyValue(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case SelectbyIndex:
				key.selectbyIndex(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;
			case InsertTag:
				key.insertTag(driver, byProperty, res, sStep, sTCMapped, sAction, sPage, sObject, sError, sData,
						sTestScenario, deviceName);
				break;

			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetKeyword function");
			System.out.println(e);
		}
	}

	public void main(WebDriver driver, String deviceName) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This is the Main function of the
			 * Framework (Starting point)
			 */
			parentWindowHandle = driver.getWindowHandle();
			getSuiteName();
			for (i = 0; i < iSuiteCount; i++) {
				res.createSummaryReport(arrSuiteName.get(i), deviceName);
				res.createSummaryTableHeader(arrSuiteName.get(i));
				getTSName();
				for (j = 0; j < iTSCount; j++) {
					key.iSummaryFlag = 0;
					loadObjects(arrObjectRepository.get(j));
					getTestSteps(res, arrTSName.get(j), driver, deviceName);
				}
				res.writeSummaryEnd();
				res.closeSummaryReport();
				arrTSName.clear();
			}
			driver.close();
			driver.quit();
			Thread.sleep(2000);
			// res.createZip();
			// res.SendEmail();
		} catch (Exception e) {
			driver.close();
			driver.quit();
			System.out.println("Ooooopppppppssss!! Error in Main function");
			System.out.println(e);
		}
	}

	public void main(WebDriver driver, String deviceName, DesiredCapabilities cap) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This is the Main function of the
			 * Framework (Starting point)
			 */
			parentWindowHandle = driver.getWindowHandle();
			getSuiteName();
			for (i = 0; i < iSuiteCount; i++) {
				res.createSummaryReport(arrSuiteName.get(i), deviceName);
				res.createSummaryTableHeader(arrSuiteName.get(i));
				getTSName();
				for (j = 0; j < iTSCount; j++) {
					key.iSummaryFlag = 0;
					loadObjects(arrObjectRepository.get(j));
					getTestSteps(res, arrTSName.get(j), driver, deviceName);
				}
				res.writeSummaryEnd();
				res.closeSummaryReport();
				arrTSName.clear();
			}
			driver.close();
			driver.quit();
			Thread.sleep(2000);
			// res.createZip();
			// res.SendEmail();
		} catch (Exception e) {
			driver.close();
			driver.quit();
			System.out.println("Ooooopppppppssss!! Error in Main function");
			System.out.println(e);
		}
	}

	public void main(UTAS_Session utasSession) {
		try {
			parentWindowHandle = utasSession.WD.getWindowHandle();
			this.driver = utasSession.WD;
			getSuiteName();
			for (i = 0; i < iSuiteCount; i++) {
				SuiteName = arrSuiteName.get(i);
				System.out.println("*********************************************");
				System.out.println("Executing Suite : " + SuiteName);
				System.out.println("*********************************************");
				res.createSummaryReport(arrSuiteName.get(i), utasSession.STR_BROWSER_OR_DEVICE);
				res.createSummaryTableHeader(arrSuiteName.get(i));
				getTSName(); // array of Test Scenarios and Array of Object
								// Repositories.
				for (j = 0; j < iTSCount; j++) {
					key.iSummaryFlag = 0;
					loadObjects(arrObjectRepository.get(j));
					// getTestSteps(res, arrTSName.get(j), utasSession.WD,
					// utasSession.STR_BROWSER_OR_DEVICE);
					getTestSteps(res, arrTSName.get(j), utasSession);
				}
				res.writeSummaryEnd();
				res.closeSummaryReport();
				arrTSName.clear();
			}
			driver.close();
			driver.quit();
			Thread.sleep(2000);
			// res.createZip();
			// res.SendEmail();
		} catch (Exception e) {
			driver.close();
			driver.quit();
			System.out.println("Ooooopppppppssss!! Error in Main function");
			System.out.println(e);
		}
	}
}
