package com.sapient.utas.scripts;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class GetCapabilities {

	public static DesiredCapabilities getFireFoxCapabilities() {
		DesiredCapabilities ffCapabilities = DesiredCapabilities.firefox();
		ffCapabilities.setJavascriptEnabled(true);
		ffCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return ffCapabilities;
	}

	public static DesiredCapabilities getIECapabilities() {
		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		ieCapabilities.setJavascriptEnabled(true);
		ieCapabilities.setCapability("requireWindowFocus", true);
		ieCapabilities.setCapability("enablePersistentHover", true);
		ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		ieCapabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return ieCapabilities;
	}

	public static DesiredCapabilities getChromeCapabilities() {
		DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
		chromeCapabilities.setJavascriptEnabled(true);
		chromeCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return chromeCapabilities;
	}

	public static DesiredCapabilities getSafariCapabilities() {
		DesiredCapabilities safariCapabilities = DesiredCapabilities.safari();
		safariCapabilities.setJavascriptEnabled(true);
		safariCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return safariCapabilities;
	}
}
