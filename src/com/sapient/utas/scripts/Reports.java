package com.sapient.utas.scripts;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Reports {
	Writer output = null;
	Writer summaryoutput = null;
	String sDateTime;
	String sStatus, sRecipient, sSubject, sMailBody, sGreeting, sMailContent, sName, sTeam, sContact;
	Properties deviceConfig;

	public void getDateTime() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will get the current
			 * Date&Time
			 */
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
			Date now = new Date();
			String strDate = sdfDate.format(now);
			String strTime = sdfTime.format(now);
			strTime = strTime.replace(":", "-");
			sDateTime = "D" + strDate + "_T" + strTime;
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public void createReport(String sFeature, String deviceName) {
		getDateTime();
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will create the Report
			 * file
			 */
			File file1 = new File("outputs/Reports/DetailedReport");
			file1.mkdirs();
			File file = new File(file1, "Execution_Report_" + sFeature + "_" + deviceName + "_" + sDateTime + ".html");
			output = new BufferedWriter(new FileWriter(file));
			output.write("<html><body>");
			output.write("<center><h2> <u>AUTOMATED TEST EXECUTION REPORT</u></h2></center>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void closeReport() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will close the report
			 * file
			 */
			output.write("</html></body>");
			output.close();
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void createTableHeader(String sTestScenario) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will create the Header
			 * of the report file
			 */
			output.write("<font face=arial size=3><b>Test Scenario Name	::	</b>  </font> <font face=arial size=2>"
					+ sTestScenario + "</font></br>");
			output.write(
					"<font face=arial size=3><b>Execution Start Time	::	</b>  </font> <font face=arial size=2><b>"
							+ sDateTime + "</b> </font></br></br>");
			// output.write("<font face=arial size=3><b>Scenario Description ::
			// </b> </font> <b> <font face=arial size=2>"
			// + sTestStep + "</b> </font></br>");
			output.write("<table cellpadding=0 cellspacing=0 width=1100 border=2 bordercolor=BLACK>");
			output.write("<tr>");
			output.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>STEPS</b></font></td>");
			output.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TC_MAPPED</b></font></td>");
			output.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>ACTION</b></font></td>");
			output.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>WINDOW</b></font></td>");
			output.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>OBJECT</b></font></td>");
			output.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TEST DATA</b></font></td>");
			output.write(
					"<td width=300 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>VERIFICATION STEP</b></font></td>");
			output.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>RESULT</b></font></td>");
			output.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeTCPass(String sStep, String sTCMapped, String sAction, String sWindow, String sObject,
			String sTestData, String sVerificationStep) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write the Pass
			 * result in the report file
			 */
			output.write("<tr>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sStep
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sTCMapped
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sAction
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sWindow
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sObject
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>" + sTestData
					+ "</font></td>");
			output.write("<td width=300 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=black>"
					+ sVerificationStep + "</font></td>");
			output.write(
					"<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green><b>PASS</b></font></td>");
			output.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeTCFail(String sStep, String sTCMapped, String sAction, String sWindow, String sObject,
			String sTestData, String sVerificationStep) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write the Fail
			 * result in the report file
			 */
			output.write("<tr>");

			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sStep
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sTCMapped
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sAction
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sWindow
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sObject
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>" + sTestData
					+ "</font></td>");
			output.write("<td width=300 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red>"
					+ sVerificationStep + "</font></td>");
			output.write(
					"<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=red><a @href=> <b>FAIL !</b> </a> <OBJECT data=D:\\Framework_Repository\\Automation_Framework\\outputs\\ErrorScreenShot\\Screenshot-Step-23.png type=png> </OBJECT></font></td>");
			output.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeTCComment(String sComment) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write a comment
			 * in the report file
			 */
			output.write("<tr>");
			output.write("<td width=100 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=100 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=150 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=100 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=150 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=100 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=300 align=middle><font face=arial size=2 color=blue><b>" + sComment
					+ "</b></font></td>");
			output.write("<td width=100 align=middle><font face=arial size=2 color=blue><b>COMMENT</b></font></td>");
			output.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeTCEnd(String sStep, String sTCMapped, String sAction, String sWindow, String sObject,
			String sTestData, String sVerificationStep) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will close the result
			 * table
			 */
			output.write("<tr>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sStep
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sTCMapped
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sAction
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sWindow
					+ "</font></td>");
			output.write("<td width=150 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sObject
					+ "</font></td>");
			output.write("<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>" + sTestData
					+ "</font></td>");
			output.write("<td width=300 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green>"
					+ sVerificationStep + "</font></td>");
			output.write(
					"<td width=100 bgcolor=#EFFBF5  align=middle><font face=arial size=2 color=green><b>END</b></font></td>");
			output.write("</tr>");
			output.write("</table>");
			output.write("</br><hr align=left width=1100>");
			output.write("</td></tr></table>");
			output.write("</br></br>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void createSummaryReport(String sSuite, String deviceName) {
		getDateTime();
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will create the
			 * Summary Report file
			 */
			File file1 = new File("outputs/Reports/SummaryReport");
			file1.mkdirs();
			File file = new File(file1, "Execution_Report_" + sSuite + "_" + deviceName + "_" + sDateTime + ".html");

			summaryoutput = new BufferedWriter(new FileWriter(file));
			summaryoutput.write("<html><body>");
			summaryoutput.write("<center><h2> <u>AUTOMATED TEST EXECUTION SUMMARY REPORT</u></h2></center>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void closeSummaryReport() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function close the Summary
			 * Report file
			 */
			summaryoutput.close();
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void createSummaryTableHeader(String sTestSuite) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write the Header
			 * of teh Summary Report file
			 */
			summaryoutput
					.write("<font face=arial size=3><b>Test Scenario Name	::	</b>  </font> <font face=arial size=2>"
							+ sTestSuite + "</font></br>");
			summaryoutput
					.write("<font face=arial size=3><b>Execution Start Time	::	</b>  </font> <font face=arial size=2><b>"
							+ sDateTime + "</b> </font></br></br>");
			summaryoutput.write("<table cellpadding=0 cellspacing=0 width=1100 border=2 bordercolor=BLACK>");
			summaryoutput.write("<tr>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>#</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TEST_SCENARIO_NAME</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TOTAL_STEPS</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TOTAL_STEPS_EXECUTED</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TOTAL_STEPS_PASS</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TOTAL_STEPS_FAIL</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>RESULT</b></font></td>");
			summaryoutput.write(
					"<td width=200 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>TOTAL_EXECUTION_TIME<br> (Seconds)</b></font></td>");
			summaryoutput.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeSummaryPass(int Seq, String sTestScenario, int sTCCount, int TSE, int TSP, int TSF, long TET) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write the Pass
			 * result in the summary report file
			 */
			summaryoutput.write("<tr>");
			summaryoutput.write("<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ Seq + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ sTestScenario + "</b></font></td>");
			summaryoutput.write("<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ sTCCount + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ (TSE + 1) + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TSP + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TSF + "</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=green><b>PASS </b></font></td>");
			summaryoutput.write("<td width=200 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TET + "</b></font></td>");
			summaryoutput.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeSummaryFail(int Seq, String sTestScenario, int sTCCount, int TSE, int TSP, int TSF, long TET) {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will write the Fail
			 * result in the summary report file
			 */
			summaryoutput.write("<tr>");
			summaryoutput.write("<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ Seq + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ sTestScenario + "</b></font></td>");
			summaryoutput.write("<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ sTCCount + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ (TSE + 1) + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TSP + "</b></font></td>");
			summaryoutput.write("<td width=150 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TSF + "</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=red><b>FAIL !! </b></font></td>");
			summaryoutput.write("<td width=200 bgcolor=#EFFBF5 align=middle><font face=arial size=2 color=black><b>"
					+ TET + "</b></font></td>");
			summaryoutput.write("</tr>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

	public void writeSummaryEnd() {
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will end teh summary
			 * report table
			 */
			summaryoutput.write("<tr>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=150 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=100 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write(
					"<td width=200 bgcolor=#0404B4 align=middle><font face=arial size=2 color=white><b>END</b></font></td>");
			summaryoutput.write("</tr>");
			summaryoutput.write("</table>");
			summaryoutput.write("</br><hr align=left width=1100>");
			summaryoutput.write("</td></tr></table>");
			summaryoutput.write("</br></br>");
		} catch (Exception e) {
			// System.err.println(e);
		}
	}

}
