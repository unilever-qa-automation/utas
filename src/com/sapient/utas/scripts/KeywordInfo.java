package com.sapient.utas.scripts;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface KeywordInfo {
	KeywordType type() default KeywordType.GENERIC;

	String description() default "";

	String parent() default "PAGENAME";

	String child() default "OBJECTNAME";

	String data() default "";

	String projectName() default "";

	public enum KeywordType {
		GENERIC, WEB_ONLY, MOBILE_ONLY, PROJECT_SPECIFIC
	}
}
