package com.sapient.utas.scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class UTAS_Session {

	public DesiredCapabilities CAPABILITIES = null;
	public WebDriver WD = null;
	public String STR_BROWSER_OR_DEVICE = null;
	public String STR_NODE_URL = null;
	public String STR_HUB_URL = null;
}
