package com.sapient.utas.scripts;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sapient.utas.d2.HelperClass;

public class Driver implements Runnable {
	private Thread t;
	private String threadName;
	static String deviceName;
	static WebDriver Tempdriver, driver = null;
	static WebElement element;
	static WebDriverWait wait;
	public DesiredCapabilities cap;
	static Properties deviceConfig;
	static String sAddDevicePort;
	static int iDevicePort = 0;
	private UTAS_Session utasSession = null;
	Utilities util = new Utilities();
	static String hostHub;
	static String portHub;

	Driver(String name) {
		threadName = name;
	}

	Driver(String name, DesiredCapabilities cap) {
		threadName = name;
	}

	Driver(UTAS_Session utasSession) {
		threadName = utasSession.STR_BROWSER_OR_DEVICE;
		this.utasSession = utasSession;
	}

	public static void getDevicesConfig() throws IOException {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : Load Config.properties file from input
		 * folder
		 */
		deviceConfig = new Properties();
		File deviceConfigfile = new File("inputs" + File.separator + "Config.properties");
		FileInputStream dcf = new FileInputStream(deviceConfigfile);
		deviceConfig.load(dcf);
	}

	public static void getDevices() throws IOException {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will enable the
		 * capabilities as per config file
		 */
		DesiredCapabilities.android();
		DesiredCapabilities androidCapabilities = DesiredCapabilities.android();
		androidCapabilities.setJavascriptEnabled(true);
		androidCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities.ipad();
		DesiredCapabilities iPadCapabilities = DesiredCapabilities.ipad();
		iPadCapabilities.setJavascriptEnabled(true);
		iPadCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities.iphone();
		DesiredCapabilities iPhoneCapabilities = DesiredCapabilities.iphone();
		iPhoneCapabilities.setJavascriptEnabled(true);
		iPhoneCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities ffCapabilities = DesiredCapabilities.firefox();
		ffCapabilities.setJavascriptEnabled(true);
		ffCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities safariCapabilities = DesiredCapabilities.safari();
		safariCapabilities.setJavascriptEnabled(true);
		safariCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		ieCapabilities.setJavascriptEnabled(true);
		ieCapabilities.setCapability("requireWindowFocus", true);
		ieCapabilities.setCapability("enablePersistentHover", true);
		ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		ieCapabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
		chromeCapabilities.setJavascriptEnabled(true);
		chromeCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		getDevicesConfig();

		hostHub = deviceConfig.getProperty("HubIP", "localhost");
		portHub = deviceConfig.getProperty("HubPort", "4444");

		sAddDevicePort = deviceConfig.getProperty("Firefox", "NA");
		if (!(sAddDevicePort.equalsIgnoreCase("NA"))) {
			deviceName = "Firefox";

			UTAS_Session utasSession = new UTAS_Session();
			utasSession.CAPABILITIES = GetCapabilities.getFireFoxCapabilities();
			utasSession.STR_HUB_URL = "http://" + hostHub + ":" + portHub + "/wd/hub";
			Tempdriver = new RemoteWebDriver(new URL(utasSession.STR_HUB_URL), utasSession.CAPABILITIES);
			driver = new Augmenter().augment(Tempdriver);
			utasSession.WD = driver;
			utasSession.STR_BROWSER_OR_DEVICE = "Firefox";
			// Driver B1 = new Driver("Firefox", ffCapabilities);

			Driver B1 = new Driver(utasSession);
			B1.start();
		}

		sAddDevicePort = deviceConfig.getProperty("Safari", "NA");
		if (!(sAddDevicePort.equalsIgnoreCase("NA"))) {
			deviceName = "Safari";
			UTAS_Session utasSession = new UTAS_Session();
			utasSession.CAPABILITIES = GetCapabilities.getSafariCapabilities();
			utasSession.STR_HUB_URL = "http://" + hostHub + ":" + portHub + "/wd/hub";
			Tempdriver = new RemoteWebDriver(new URL(utasSession.STR_HUB_URL), utasSession.CAPABILITIES);
			driver = new Augmenter().augment(Tempdriver);
			utasSession.WD = driver;
			utasSession.STR_BROWSER_OR_DEVICE = "Safari";
			Driver B2 = new Driver(utasSession);
			B2.start();
		}

		sAddDevicePort = deviceConfig.getProperty("IE", "NA");
		if (!(sAddDevicePort.equalsIgnoreCase("NA"))) {
			deviceName = "IE";
			UTAS_Session utasSession = new UTAS_Session();
			utasSession.CAPABILITIES = GetCapabilities.getIECapabilities();
			utasSession.STR_HUB_URL = "http://" + hostHub + ":" + portHub + "/wd/hub";
			Tempdriver = new RemoteWebDriver(new URL(utasSession.STR_HUB_URL), utasSession.CAPABILITIES);
			driver = new Augmenter().augment(Tempdriver);
			utasSession.WD = driver;
			utasSession.STR_BROWSER_OR_DEVICE = "IE";
			Driver B3 = new Driver(utasSession);
			B3.start();
		}

		sAddDevicePort = deviceConfig.getProperty("Chrome", "NA");
		if (!(sAddDevicePort.equalsIgnoreCase("NA"))) {

			deviceName = "Chrome";
			UTAS_Session utasSession = new UTAS_Session();
			utasSession.CAPABILITIES = GetCapabilities.getChromeCapabilities();
			utasSession.STR_HUB_URL = "http://" + hostHub + ":" + portHub + "/wd/hub";
			Tempdriver = new RemoteWebDriver(new URL(utasSession.STR_HUB_URL), utasSession.CAPABILITIES);
			driver = new Augmenter().augment(Tempdriver);
			driver.manage().window().maximize();
			utasSession.WD = driver;
			utasSession.STR_BROWSER_OR_DEVICE = "Chrome";
			Driver B4 = new Driver(utasSession);
			B4.start();
		}

	}

	@Override
	public void run() {
		try {
			deviceName = threadName;
			// util.main(driver, deviceName);
			util.main(utasSession);

			System.out.println("End of Execution");
		} catch (Exception e) {
		} finally {
			HelperClass.killServers();
		}
	}

	public void start() throws IOException {
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}

	public static void main(String args[]) {
		if ((args.length > 0) && args[0].toUpperCase().contains("GETKEYWORDS")) {
			GetKeywordList.main(null);
		} else {
			try {
				getDevices();
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}

	static {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\WebDriverDrivers\\chromedriver.exe");
		System.setProperty("webdriver.gecko.driver",
				System.getProperty("user.dir") + "\\WebDriverDrivers\\geckodriver.exe");
	}
}
