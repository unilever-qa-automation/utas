package com.sapient.utas.scripts;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.TreeMap;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class GetKeywordList {

	public static void main(String[] args) {

		com.sapient.utas.scripts.Keywords keywords = new com.sapient.utas.scripts.Keywords();
		Method[] methods = keywords.getClass().getMethods();
		TreeMap<String, KeywordInfo> treeMap = new TreeMap<String, KeywordInfo>();
		try {
			File exlFile = new File("KeywordsList.xls");
			WritableWorkbook writableWorkbook = Workbook.createWorkbook(exlFile);
			WritableSheet writableSheet = writableWorkbook.createSheet("Keywords", 0);

			WritableFont headingFont = new WritableFont(WritableFont.ARIAL, 14);
			headingFont.setColour(Colour.WHITE);
			WritableCellFormat headingFormat = new WritableCellFormat(headingFont);
			headingFormat.setBackground(Colour.BLUE_GREY);
			headingFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

			Label keyword = new Label(0, 0, "KEYWORD", headingFormat);
			Label Page = new Label(1, 0, "PAGENAME", headingFormat);
			Label Object = new Label(2, 0, "OBJECTNAME", headingFormat);
			Label Data = new Label(3, 0, "DATA", headingFormat);
			Label Type = new Label(4, 0, "TYPE", headingFormat);
			Label ProjectName = new Label(5, 0, "PROJECTNAME", headingFormat);
			Label Desc = new Label(6, 0, "DESCRIPTION", headingFormat);

			writableSheet.addCell(keyword);
			writableSheet.addCell(Page);
			writableSheet.addCell(Object);
			writableSheet.addCell(Data);
			writableSheet.addCell(Type);
			writableSheet.addCell(ProjectName);
			writableSheet.addCell(Desc);

			int row = 1;

			for (Method method : methods) {
				if (method.isAnnotationPresent(KeywordInfo.class)) {
					System.out.println("Name : " + method.getName());
					Annotation annotation = method.getAnnotation(KeywordInfo.class);
					KeywordInfo keywordInfo = (KeywordInfo) annotation;
					treeMap.put(Character.toUpperCase(method.getName().charAt(0)) + method.getName().substring(1),
							keywordInfo);
					System.out.println("Parent : " + keywordInfo.parent());
					System.out.println("Object : " + keywordInfo.child());
					System.out.println("Data : " + keywordInfo.data());
					System.out.println("Project Name : " + keywordInfo.projectName());
					System.out.println("Description : " + keywordInfo.description());
					System.out.println("\n\n");

				}
			}

			row = 1;
			for (String key : treeMap.keySet()) {
				KeywordInfo keywordInfo = treeMap.get(key);

				// Create Cells with contents of different data types.
				// Also specify the Cell coordinates in the constructor
				keyword = new Label(0, row, key);
				Page = new Label(1, row, keywordInfo.parent());
				Object = new Label(2, row, keywordInfo.child());
				Data = new Label(3, row, keywordInfo.data());
				Type = new Label(4, row, keywordInfo.type().toString());
				ProjectName = new Label(5, row, keywordInfo.projectName());
				Desc = new Label(6, row, keywordInfo.description());

				// Add the created Cells to the sheet
				writableSheet.addCell(keyword);
				writableSheet.addCell(Page);
				writableSheet.addCell(Object);
				writableSheet.addCell(Data);
				writableSheet.addCell(Type);
				writableSheet.addCell(ProjectName);
				writableSheet.addCell(Desc);

				row++;
			}

			// Write and close the workbook
			writableWorkbook.write();
			writableWorkbook.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}

	}
}
