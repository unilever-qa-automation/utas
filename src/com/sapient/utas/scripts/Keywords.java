package com.sapient.utas.scripts;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;
import com.sapient.utas.action.UTASAction;
import com.sapient.utas.d2.exceptions.D2Exception;
import com.sapient.utas.d2.helpers.HelperUtils;
import com.sapient.utas.d2.helpers.SearchResult;
import com.sapient.utas.d2.helpers.SearchResultComparator;
import com.sapient.utas.scripts.KeywordInfo.KeywordType;

import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Keywords {
	public Keywords() {
		super();
	}

	public Keywords(UTASAction action) {
		super();
		this.action = action;
	}

	UTASAction action = null;
	Workbook creativeWorkbook;
	Sheet creativeWorksheet;
	WritableWorkbook creativeOPWorkbook = null;
	WritableSheet creativeOPWorksheet = null;
	String sStep, sAction, sPage, sObject, sData, sTCMapped, sTestScenario;
	String sStoredValue, sUrl, xpath, sProperty, sValue, sActualValue, sHeader;
	String deviceName, sCreativeDateTime;
	Properties objectRepository;
	By byProperty;
	Label label;
	Writer output = null;
	WebDriverWait wait, alertwait;
	WebDriver driver;
	WebElement element;
	int creativeCC, creativeRC, iSummaryFlag = 0;
	int iTCLoop, iClickFlag, iFlag;
	int iTCRowCount;
	ArrayList<String> arrOnError = new ArrayList<>();
	WritableFont wfontStatus;
	int iTotalStepsPass = 0, iTotalStepsFail = 0;
	int iActualXCoordinate, iActualYCoordinate, iActualHeight, iActualWidth, iRelativeXCoordinate, iRelativeYCoordinate,
			iRelativeHeight, iRelativeWidth;

	int count = 0;
	Robot robot;
	javax.swing.Timer timer;

	public void TakeScreenshotTest(WebDriver driver, String sStep, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will take the Screen Shot
		 * of the current page view
		 */

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			String filename = sTestScenario + "_" + deviceName + "_" + "Screenshot-" + sStep + ".png";
			FileUtils.copyFile(scrFile, new File("outputs/ErrorScreenShot/" + filename));
		} catch (Exception e) {
			System.out.println("Oooops!!! encountered somme error while capturing screenshot");
			System.out.println(e);
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword is used to delete all the cookies.", projectName = "", type = KeywordType.WEB_ONLY)
	public void deleteCookies(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will delete the previous
		 * cookies
		 */
		try {
			driver.manage().deleteAllCookies();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Delete Cookies");
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in DeleteCookies Keyword");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in DeleteCookies Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(type = KeywordType.WEB_ONLY, parent = "..", child = "..", description = "This keyword is used to click at Browse Back button of browser. Only works in Web Browsers.")
	public void browseBack(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Richa Agarwal (ragarwal113@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will Browse Back to the
		 * Previous page
		 */
		try {
			driver.navigate().back();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Browse Back");
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in BrowseBack Keyword");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occured in BrowseBack Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword presses the forward button of browser.", projectName = "", type = KeywordType.WEB_ONLY)
	public void browseForward(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Richa Agarwal (ragarwal113@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will Browse Forward to the
		 * Next page
		 */
		try {
			driver.navigate().forward();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Browse Forward");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in BrowseForward Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword refreshes the browser.", projectName = "", type = KeywordType.WEB_ONLY)
	public void refresh(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Richa Agarwal (ragarwal113@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will Refresh the currently
		 * opened page
		 */
		try {
			driver.navigate().refresh();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Browser Refresh");
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in Refresh Keyword");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in BrowseForward Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "..", description = "Use this keyword to wait until page is loaded. This means DOM is loaded, It does not wait for any AJAX scripts to complete", projectName = "D2", type = KeywordType.WEB_ONLY)
	public void WaitForPageLoad(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar 12 (skumar213@sapient.com) Last Modified:
		 * 9-June-2016 Description : Use this keyword to wait until page is
		 * loaded
		 */
		try {
			boolean flag = true;
			do {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				String value = (String) executor.executeScript("return document.readyState");
				if (value.equalsIgnoreCase("complete")) {
					flag = false;
				}
				Thread.sleep(2000);
			} while (flag);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Page is loaded");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in WaitForPageLoad function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occured");
			iSummaryFlag = 1;
			e.printStackTrace();
		}

	}

	@KeywordInfo(parent = "..", child = "..", data = "..", description = "Use this keyword to wait until element is disappeared. takes time in miliseconds", projectName = "D2", type = KeywordType.WEB_ONLY)
	public void waitForElementDisappear(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar 12 (skumar213@sapient.com) Last Modified:
		 * 9-June-2016 Description : Use this keyword to wait until page is
		 * loaded
		 */
		try {

			WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(sData));
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byProperty));
			Thread.sleep(1000);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Page is loaded");
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in waitForElementDisappear function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occured");
			iSummaryFlag = 1;
			e.printStackTrace();
		}

	}

	@KeywordInfo(parent = "..", child = "..", data = "url", description = "This keyword navigates to the url mentioned in the data column.", projectName = "", type = KeywordType.WEB_ONLY)
	public void launch(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will Launch the
		 * application
		 */
		try {

			if (driver == null) {

			} else {

			}

			driver.manage().deleteAllCookies();
			// driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			Thread.sleep(5000);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Application is launched");
			if (deviceName.contains("Phone")) {
				driver.get(sData);
				Thread.sleep(30000);
			} else if (deviceName.contains("Tab")) {
				driver.get(sData);
				Thread.sleep(30000);
			} else {
				if (sError.equalsIgnoreCase("BasicAuthentication")) {
					if (deviceName.equalsIgnoreCase("IE")) {
						driver.get(sPage + sData);
					} else {
						driver.get(sPage + sObject + sData);
					}
				} else {
					driver.get(sData);
					Thread.sleep(5000);
				}
				driver.manage().window().maximize();
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in Launch function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in BrowseForward Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword handles the ssl issue of IE browser.", projectName = "", type = KeywordType.WEB_ONLY)
	public void ieSSL(WebDriver driver, Reports rep, String sStep, String sTCMapped, String sAction, String sError,
			String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will take care of the SSL
		 * certificate issue for Internet Explorer
		 */
		try {
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..",
					"Continue link is clicked on IE and is not applicable to other browsers");
			if (deviceName.equalsIgnoreCase("IE")) {
				driver.get("javascript:document.getElementById('overridelink').click()");
				Thread.sleep(5000);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in ieSSL function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in BrowseForward Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword presses enter button of keyboard.", projectName = "", type = KeywordType.WEB_ONLY)

	public void confirmAlert(WebDriver driver, Reports rep, String sStep, String sTCMapped, String sAction,
			String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will confirm the alert
		 * pop-up using Robot function
		 */
		try {

			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Alert is confirmed if avialable");
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(5000);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Successful");
		} catch (Exception e) {
			// System.out.println("Ooooopppppppssss!! Error in confirmAlert
			// function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in confirmAlert Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
		}
	}

	@KeywordInfo(parent = "widthdimension", child = "..", data = "heightdimension", description = "This keyword resizes the browser window", projectName = "", type = KeywordType.WEB_ONLY)
	public void resizeWindow(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will resize the opened
		 * windows as per the dimension provided
		 */
		try {
			driver.manage().window().setSize(new Dimension(Integer.parseInt(sObject), Integer.parseInt(sData)));
			Thread.sleep(2000);
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Explicit Wait for 10 second");
			iTotalStepsPass = iTotalStepsPass + 1;
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in Resize function");
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
					"Exception occured in BrowseForward Keyword");
			iSummaryFlag = 1;
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "milliseconds", description = "This keyword stops execution for milliseconds equivalent.", projectName = "", type = KeywordType.WEB_ONLY)
	public void wait(WebDriver driver, Reports rep, String sStep, String sTCMapped, String sAction, String sError,
			String sData) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will suspend the execution
		 * Thread for the given time frame
		 */
		try {
			Thread.sleep(Integer.parseInt(sData));
			// Thread.sleep(2000);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, "..", "..", "..", "Implicit Wait for " + sData + " millisecond");
			iTotalStepsPass = iTotalStepsPass + 1;
		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "Object", data = "Key to press from Keys class <https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/Keys.html>", description = "this sends keys to element", projectName = "Unilever", type = KeywordType.WEB_ONLY)
	public void sendKey(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		try {
			iClickFlag = 0;
			wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.elementToBeClickable(byProperty));
			if (element.isEnabled()) {
				// flash(driver, element);
				element.sendKeys(Keys.valueOf(sData.toUpperCase()));
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + ": webelement is clicked");
				Thread.sleep(500);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not enabled");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception occured in clicking object - " + sObject);
				iSummaryFlag = 1;
				System.out.println("Exception occured in clicking object - " + sObject);
				e.printStackTrace();
			}
		}

	}

	@KeywordInfo(parent = "PageName", child = "Object", data = "", description = "This keyword clicks on the webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void click(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {
			iClickFlag = 0;
			wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.elementToBeClickable(byProperty));
			// flash(driver, element);
			iClickFlag = 1;
			element.click();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + ": webelement is clicked");
			if (deviceName.contains("Tab")) {
				Thread.sleep(10000);
			}
			if (deviceName.contains("Phone")) {
				Thread.sleep(10000);
			}
			Thread.sleep(500);

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception occured in clicking object - " + sObject);
				iSummaryFlag = 1;
				System.out.println("Exception occured in clicking object - " + sObject);
				e.printStackTrace();
			}
		}
	}

	// private void waitUntilClickAble(WebDriver driver, WebElement element) {
	// FluentWait<WebDriver> wait = new
	// FluentWait<WebDriver>(driver).pollingEvery(200, TimeUnit.MILLISECONDS)
	// .withTimeout(60, TimeUnit.SECONDS);
	// wait.until(new Predicate<WebDriver>() {
	// @Override
	// public boolean apply(WebDriver arg0) {
	// try {
	// element.click();
	// return true;
	// } catch (Exception e) {
	// }
	// return false;
	// }
	// });
	// }

	@KeywordInfo(parent = "PageName", child = "TableObject", data = "<Row>,<Col>,<linktext>", description = "This keyword clicks on the link present in particular table cell on the basis of link text. Row/col index starts from 1.", projectName = "", type = KeywordType.WEB_ONLY)
	public void clickTableCellLinkByText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {

			int intRow = Integer.parseInt(sData.split(",")[0]);
			int intCol = Integer.parseInt(sData.split(",")[1]);
			String expectedText = sData.split(",")[2].trim();

			iClickFlag = 0;
			wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));

			List<WebElement> rows = element.findElements(By.xpath("./*[self::thead | self::tbody]/tr"));
			WebElement row = rows.get(intRow - 1);
			List<WebElement> cols;
			cols = row.findElements(By.xpath("./*[self::th | self:: td]"));
			WebElement link = cols.get(intCol - 1).findElement(By.linkText(expectedText));

			// // flash(driver, element);
			iClickFlag = 1;
			link.click();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + ": webelement is clicked");
			if (deviceName.contains("Tab")) {
				Thread.sleep(10000);
			}
			if (deviceName.contains("Phone")) {
				Thread.sleep(10000);
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception occured in clicking object - " + sObject);
				iSummaryFlag = 1;
				System.out.println("Exception occured in clicking object - " + sObject);
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "TableObject", data = "<Row>,<Col>,<linkIndex>", description = "This keyword clicks on the link present in particular table cell on the basis of Index. Row/col/Link index starts from 1.", projectName = "", type = KeywordType.WEB_ONLY)
	public void clickTableCellLinkByIndex(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {

			int intRow = Integer.parseInt(sData.split(",")[0]);
			int intCol = Integer.parseInt(sData.split(",")[1]);
			int linkIndex = Integer.parseInt(sData.split(",")[2]);

			iClickFlag = 0;
			wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));

			List<WebElement> rows = element.findElements(By.xpath("./*[self::thead | self::tbody]/tr"));
			WebElement row = rows.get(intRow - 1);
			List<WebElement> cols;
			cols = row.findElements(By.xpath("./*[self::th | self:: td]"));
			List<WebElement> links = cols.get(intCol - 1).findElements(By.tagName("a"));
			WebElement link = links.get(linkIndex - 1);

			// // flash(driver, element);
			iClickFlag = 1;
			link.click();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..",
					sObject + ": Link with index " + linkIndex + " is clicked");
			if (deviceName.contains("Tab")) {
				Thread.sleep(10000);
			}
			if (deviceName.contains("Phone")) {
				Thread.sleep(10000);
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception occured in clicking link with index mentioned in - " + sData);
				iSummaryFlag = 1;
				System.out.println("Exception occured in clicking link with index mentioned in - " + sData);
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword closses all the browsers window opened by current session.", projectName = "", type = KeywordType.WEB_ONLY)
	public void closeBrowser(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {
			iClickFlag = 0;
			// driver.close();
			driver.quit();
			driver = null;
			iClickFlag = 1;
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "WebDriver is successfully closed.");
			if (deviceName.contains("Tab")) {
				Thread.sleep(10000);
			}
			if (deviceName.contains("Phone")) {
				Thread.sleep(10000);
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Error in closing WebDriver.");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception in closing WebDriver.");
				iSummaryFlag = 1;
				System.out.println("Exception occured in clicking object - " + sObject);
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "locatervalue", description = "This keyword clicks on object dynamically. We can provide locater value in the Datacolumn instead of storing in OR.", projectName = "", type = KeywordType.WEB_ONLY)
	public void clickDynamicObject(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {
			iClickFlag = 0;
			wait = new WebDriverWait(driver, 60);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			if (element.isEnabled()) {
				// // flash(driver, element);
				iClickFlag = 1;
				element.click();
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + ": webelement is clicked");
				if (deviceName.contains("Tab")) {
					Thread.sleep(10000);
				}
				if (deviceName.contains("Phone")) {
					Thread.sleep(10000);
				}

			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not enabled");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception in ClickDynamicObject Keyword.");
				iSummaryFlag = 1;
				System.out.println("Exception in ClickDynamicObject Keyword.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "This keyword helps in clicking OK button of Alert PopUp.", type = KeywordType.WEB_ONLY)
	public void acceptAlert(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will confirm the Alert
		 * pop-up
		 */
		try {
			iClickFlag = 0;
			wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "Alert is accepted");
			iClickFlag = 1;

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Alert not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "It submits the form on the page.", projectName = "", type = KeywordType.WEB_ONLY)
	public void submit(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will submit the current
		 * web page
		 */
		try {

			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			if (element.isEnabled()) {
				// // flash(driver, element);
				iClickFlag = 1;
				element.submit();
				iClickFlag = 0;
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "webpage is submited");
				if (deviceName.contains("Tab")) {
					Thread.sleep(5000);
				}
				if (deviceName.contains("Phone")) {
					Thread.sleep(5000);
				}
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "It performs right click operation on webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void rightMouseClick(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Richa Agarwal (ragarwal113@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will context click (Right
		 * Click) on the element
		 */
		try {

			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.elementToBeClickable(byProperty));
			// // flash(driver, element);
			Actions action = new Actions(driver);
			action.contextClick(element).build().perform();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "element is context clicked");
			Thread.sleep(500);
		} catch (Exception e) {
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "sObject not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Value to be entered as inner text of html tag.", description = "It enters data in the  webelement.", projectName = "D2", type = KeywordType.GENERIC)
	public void enterHeroMainCopyText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			element.click();
			Thread.sleep(2000);
			List<WebElement> elements = element.findElements(By.tagName("p"));
			if (elements.size() < 1) {
				throw new Exception("Error occor while finding element");
			}
			element = elements.get(0);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerText='" + sData.trim() + "'", element);

			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
					sData + " is entered in webelement " + sObject);
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}

	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Value to be entered as inner text of html tag.", description = "It enters data in the  webelement.", projectName = "D2", type = KeywordType.GENERIC)
	public void richText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			element.click();
			Thread.sleep(500);
			List<WebElement> elements = element.findElements(By.tagName("p"));
			if (elements.size() < 1) {
				throw new Exception("Error occor while finding element");
			}
			element = elements.get(0);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerText='" + sData.trim() + "'", element);

			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
					sData + " is entered in webelement " + sObject);
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Value to be entered as inner text of html tag.", description = "It enters data in the  webelement.", projectName = "D2", type = KeywordType.GENERIC)
	public void enterHeroIntroCopyText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			element.click();
			Thread.sleep(2000);
			List<WebElement> elements = element.findElements(By.tagName("p"));
			if (elements.size() < 1) {
				throw new Exception("Error occor while finding element");
			}
			element = elements.get(0);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerText='" + sData.trim() + "'", element);

			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
					sData + " is entered in webelement " + sObject);
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = ".", description = "filters product .", projectName = "D2", type = KeywordType.GENERIC)
	public void productFilter(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			driver.findElements(By
					.xpath("(.//div[@class='c-search-listing__wrap']//li[contains(@class,'c-search-listing__item') and contains(@class,'"
							+ sData.trim() + "')])"));
			List<WebElement> list = HelperUtils.loadAllElements(driver);
			String text = driver.findElements(By.cssSelector("select.c-search-filters__select option")).get(1)
					.getText();
			int x = Integer.parseInt(text.substring(text.indexOf("(") + 1, text.indexOf(")")));
			if (list.size() == x)
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is entered in webelement " + sObject);
			else {
				throw new Exception("Result Items count and number in dropdown is not same");
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Value to be entered in textbox/textarea.", description = "It enters data in the textbox/textarea webelement.", projectName = "", type = KeywordType.GENERIC)
	public void enter(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			element.clear();
			element.sendKeys(sData.trim());
			// element.sendKeys(Keys.TAB);
			Thread.sleep(500);
			iFlag = 1;
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
					sData + " is entered in webelement " + sObject);
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "It clears data from the textbox/textarea webelement.", projectName = "", type = KeywordType.GENERIC)
	public void clear(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will clear the entered
		 * text from text field
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " is cleared");
			element.clear();
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "text to be selected in combo/list box or ul", description = "This keyword works for both select and ul objects. It selects item from listbox/combobox/ul-li on the basis of text mentioned in Data column", projectName = "", type = KeywordType.GENERIC)
	public void select(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will select a value from
		 * the Drop down using Visible Text
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			if (element.getTagName().equalsIgnoreCase("UL")) {
				element.findElement(By.xpath("//descendant-or-self::*[text()='" + sData + "']")).click();
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is selected in UL/Li " + sObject);
				iFlag = 1;

			} else if (element.getTagName().equalsIgnoreCase("SELECT")) {
				new Select(element).selectByVisibleText(sData);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is selected in drop down " + sObject);
				iFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " is neither select nor UI."
						+ sAction + " only works for object having tagname select or ul");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "text to be selected in combo/list box  by value", description = "This keyword works for both select and ul objects. It selects item from listbox/combobox/ul-li on the basis of text mentioned in Data column", projectName = "", type = KeywordType.GENERIC)
	public void selectbyValue(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will select a value from
		 * the Drop down using Visible Text
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			if (element.getTagName().equalsIgnoreCase("SELECT")) {
				new Select(element).selectByValue(sData);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is selected in drop down " + sObject);
				iFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sObject + " is neither select." + sAction + " only works for object having tagname select");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "text to be selected in combo/list box  by index", description = "This keyword works for both select and ul objects. It selects item from listbox/combobox/ul-li on the basis of text mentioned in Data column", projectName = "", type = KeywordType.GENERIC)
	public void selectbyIndex(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will select a value from
		 * the Drop down using Visible Text
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			if (element.getTagName().equalsIgnoreCase("SELECT")) {
				new Select(element).selectByIndex(Integer.parseInt(sData));
				element.sendKeys(Keys.TAB);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is selected in drop down " + sObject);
				iFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sObject + " is neither select" + sAction + " only works for object having tagname select ");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "It performs mouseover action on element. Doesn't work of Safari as yet.", projectName = "", type = KeywordType.GENERIC)
	public void hover(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will mouse hover on
		 * element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);

			if (deviceName.equalsIgnoreCase("Safari")) {
				String javaScript = "var evObj = document.createEvent('MouseEvents');"
						+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
						+ "arguments[0].dispatchEvent(evObj);";

				Thread.sleep(1000);
				JavascriptExecutor javascript = (JavascriptExecutor) driver;
				Thread.sleep(1000);
				javascript.executeScript(javaScript, element);

			} else {
				new Actions(driver).moveToElement(element).build().perform();
			}
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " is hovered");
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();

			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "It perform click operation on automation object.", projectName = "", type = KeywordType.GENERIC)
	public void clickAt(WebDriver driver, By byProperty, Reports rep, String sError, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will click at 1,1
		 * co-ordinate of the element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// // flash(driver, element);
			Actions build = new Actions(driver);
			build.moveToElement(element, 10, 10).click().build().perform();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " is clicked");
			iFlag = 1;
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "Number or Name", data = "<value of name attribute or Index starting from 0.>", description = "It is used to switch inside any IFrame.", projectName = "", type = KeywordType.WEB_ONLY)
	public void switchFrame(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will switch the frame
		 * using frame number or frame name
		 */
		try {
			if (sObject.equalsIgnoreCase("Number")) {
				driver.switchTo().frame(Integer.parseInt(sData));
			} else if (sObject.equalsIgnoreCase("Name")) {
				driver.switchTo().frame(sData);
			}
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..", "Frame is switched");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Frame is not switched");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "Selects either the first frame on the page, or the main document when a page contains iframes.", projectName = "", type = KeywordType.WEB_ONLY)
	public void switchDefaultContent(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario) {
		try {
			driver.switchTo().defaultContent();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..", "Successfully switched to default content");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Exception in SwitchDefaultContent");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "Change focus to the parent context/Frame.", projectName = "", type = KeywordType.WEB_ONLY)
	public void switchParentFrame(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario) {
		try {
			driver.switchTo().parentFrame();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..", "Successfully switched to parent content");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Exception in SwitchParentFrame");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "Title or Name or Index", data = "tile value or name value or index starting with 1", description = "It is used to switch to the new browser window depending on the value mentioned in Object and Data Column. If you have object column text 'Title' then in data column you should write the title of the new window. Make sure the new window title is unique. If you have object column text 'Name' then in data column you should write the name of the new window. Make sure the new window name is unique. If you have object column text 'Index' then in data column you should write the index of the new window. Make sure the new window index is >= 0.", projectName = "", type = KeywordType.WEB_ONLY)
	public void switchWindow(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will switch the frame
		 * using frame number or frame name
		 */
		try {
			if (sObject.equalsIgnoreCase("Name")) {
				driver.switchTo().window(sData.trim());
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..",
						"Successfully switched to window having name " + sData);
			} else if (sObject.equalsIgnoreCase("Index")) {
				Set<String> handles = driver.getWindowHandles();
				int i = 1;
				int windowIndex = Integer.valueOf(sData);
				boolean windowFound = false;
				if (windowIndex <= handles.size()) {
					for (String handle : handles) {
						if (i == windowIndex - 1) {
							windowFound = true;
							driver.switchTo().window(handle);
							iTotalStepsPass = iTotalStepsPass + 1;
							rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..",
									"Successfully switched to window having name " + sData);
							break;
						}
					}
				}
				if (!windowFound) {
					iTotalStepsFail = iTotalStepsFail + 1;
					TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
					rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..",
							"Window with index " + windowIndex + " doesn't exist.");
					iSummaryFlag = 1;
				}
			} else if (sObject.equalsIgnoreCase("Title")) {
				String parentHandle = driver.getWindowHandle();
				Set<String> handles = driver.getWindowHandles();
				boolean windowFound = false;
				for (String handle : handles) {

					if (driver.switchTo().window(handle).getTitle().contains(sData)) {
						windowFound = true;

						iTotalStepsPass = iTotalStepsPass + 1;
						rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..",
								"Successfully switched to window having title contains " + sData);
						break;
					} else {
						driver.switchTo().window(parentHandle);
					}

				}
				if (!windowFound) {
					iTotalStepsFail = iTotalStepsFail + 1;
					TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
					rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..",
							"Window with title contains " + sData + " doesn't exist.");
					iSummaryFlag = 1;
				}

			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Frame is not switched");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "", description = "It is used to switch to the parent browser window", projectName = "", type = KeywordType.WEB_ONLY)
	public void switchParentWindow(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will switch the frame
		 * using frame number or frame name
		 */
		try {
			driver.switchTo().window(sData.trim());
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..", "Successfully switched to parent window");

		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Frame is not switched");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "Page", child = "FromObject", data = "ToObject", description = "perform drag and drop operation. Please mention source object in object column and target oject in data column", projectName = "", type = KeywordType.WEB_ONLY)
	public void dragAndDrop(WebDriver driver, By byPropertyFrom, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, By byPropertyTo, String sTestScenario) {
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byPropertyFrom));
			WebElement to = wait.until(ExpectedConditions.presenceOfElementLocated(byPropertyTo));
			Actions action = new Actions(driver);
			// action.clickAndHold(element).moveToElement(to).release(element).build().perform();
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);", to);
			action.dragAndDrop(element, to).build().perform();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..",
					"Successfully performed drag and drop operation");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", "Exception in drag and drop operation");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "..", child = "..", data = "Title text to verified", description = "This keyword is used for verifying the title of the webpage.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyTitle(WebDriver driver, Reports rep, String sStep, String sTCMapped, String sAction, String sPage,
			String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the title of
		 * the open webpage
		 */
		try {
			if (sData.equalsIgnoreCase(driver.getTitle())) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, "..", "..", sPage + " title is correct");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, "..", "..", sPage + "title is not correct");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to check whether the web object is enabled. It will pass if enabled and fail if disabled.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyElementEnabled(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify that the
		 * element is enabled on the webpage
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 10);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			iFlag = 1;
			if (element.isEnabled()) {
				// // flash(driver, element);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..",
						sObject + " is available and status is Enabled on" + sPage);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						sObject + " is not available on" + sPage);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to check whether the web object is disabled. It will pass if disabled and fail if enabled.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyElementDisabled(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify that the
		 * element is disabled on the webpage
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			iFlag = 1;
			if (!(element.isEnabled())) {
				// flash(driver, element);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..",
						sObject + " is available and status is Disabled on" + sPage);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						sObject + " is not available on" + sPage);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "<expected count number>", description = "This keyword is used to verify the total count of elements present..", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyElementCount(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify that the
		 * element is disabled on the webpage
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			List<WebElement> elements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byProperty));
			iFlag = 1;
			if ((elements.size() == Integer.valueOf(sData))) {

				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sObject + " is available and status is Disabled on" + sPage);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "Elements count is mismatching");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (NumberFormatException e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sData + " is not number");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to verify if a webelement is present or not.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyElementNotPresent(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify that the given
		 * element is not present on the webpage
		 */
		try {
			element = null;
			iFlag = 0;
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, 5);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			iFlag = 1;
			if (element != null || element.isEnabled()) {

				throw new Exception("Element is present");
			} else {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Element is not present");
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " is available on" + sPage);
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			iSummaryFlag = 1;
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Expected Text", description = "This keyword is used to verify the inner text of a webelement. It validates the exact text of the webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the text of
		 * the given element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			String sText = element.getText().toString().trim();
			iFlag = 1;
			if (sData.trim().equals(sText)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "Text is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sText);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "Expected Text", description = "This keyword is used to verify the inner text of a webelement contains the expected Text. It works as Instring function.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyPartOfText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the text of
		 * the given element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			String sText = element.getText().toString().trim();
			iFlag = 1;
			if (sText.contains(sData.trim())) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Expected value is:: '" + sData + "'" + " is present in Actual value :: '" + sText + "'");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sText);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "TableObjectName", data = "<RowNumber>,<ColumnNumber>,<Expected data in table cell>", description = "This keyword is used to verify the text of the table cell data. Row/column starts from 1", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyTableCellData(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		int intRow = Integer.parseInt(sData.split(",")[0]);
		int intCol = Integer.parseInt(sData.split(",")[1]);
		String expectedData = sData.split(",")[2].trim();
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			List<WebElement> rows = element.findElements(By.xpath("./*[self::thead | self::tbody]/tr")); // ./child::*/tr
			WebElement row = rows.get(intRow - 1);
			List<WebElement> cols;
			cols = row.findElements(By.xpath("./*[self::th | self:: td]"));
			WebElement col = cols.get(intCol - 1);
			sValue = col.getText();
			if (sValue.contains(expectedData)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Table cell Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}

		} catch (Exception e) {

			iTotalStepsFail = iTotalStepsFail + 1;
			// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
			iSummaryFlag = 1;
			System.out.println("Exception occurred.");
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount;
			 *
			 * }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "TableObjectName", data = "<RowNumber>,<ColumnNumber>,<innertext of link>", description = "This keyword is used to verify the presence of link within the Table cell. Row/column index starts from 1. E.g., first row and first column is represented by (1,1) ", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyTableCellLinkByText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		int intRow = Integer.parseInt(sData.split(",")[0]);
		int intCol = Integer.parseInt(sData.split(",")[1]);
		String expectedData = sData.split(",")[2].trim();
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			List<WebElement> rows = element.findElements(By.xpath("./*[self::thead | self::tbody]/tr"));
			WebElement row = rows.get(intRow - 1);
			List<WebElement> cols;
			cols = row.findElements(By.xpath("./*[self::th | self:: td]"));
			WebElement col = cols.get(intCol - 1);
			WebElement link = col.findElement(By.linkText(expectedData));
			if (link.isDisplayed()) {
				// flash(driver, link);
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Table cell contains link with Text " + expectedData);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, col, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Table cell doesn't contain link with Text " + expectedData);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}

		} catch (Exception e) {

			iTotalStepsFail = iTotalStepsFail + 1;
			// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
			iSummaryFlag = 1;
			System.out.println("Exception occurred.");
			e.printStackTrace();
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount;
			 *
			 * }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "TextboxObjectName", data = "Expected value", description = "This keyword is used to verify value of textbox.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyValue(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the Value
		 * attribute of the element
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			String sValue = element.getAttribute("value");
			if (sData.equalsIgnoreCase(sValue)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "<attribute name>;<Expected value of attribute>", description = "This keyword is used to verify the attibute value of any object.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyAttribute(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);

			String attributeName = sData.split(";")[0];
			String attributeValue = sData.split(";")[1];
			element.getTagName();
			String sValue = element.getAttribute(attributeName);
			if (attributeValue.equalsIgnoreCase(sValue)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "expected selected item in List", description = "This keyword is used to verify the selected item in the listbox/combobox", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyListItemSelected(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			Select SelectElem = new Select(element);
			sValue = SelectElem.getFirstSelectedOption().getText().trim();
			if (sValue.equals(sData.trim())) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "expected item in List", description = "This keyword is used to verify the item presence in listbox/combobox", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyListItem(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			List<WebElement> options = null;

			if (element.getTagName().equalsIgnoreCase("Select")) {
				// flash(driver, element);
				Select SelectElem = new Select(element);
				options = SelectElem.getOptions();

			} else if (element.getTagName().equalsIgnoreCase("UL")) {
				options = element.findElements(By.xpath("./li"));

			}
			boolean matchFound = false;
			for (int i = 0; i < options.size(); i++) {
				if (sData.trim().equals(options.get(i).getText().trim())) {
					matchFound = true;
					break;
				}
			}
			if (matchFound == true) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, sData + " is present as list option");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						sData + " is not present as list option");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}

		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "expected item in List", description = "This keyword is used to verify the absence of item in the listbox/combobox", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyListItemNotPresent(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			Select SelectElem = new Select(element);
			List<WebElement> options = SelectElem.getOptions();
			boolean matchFound = false;
			for (int i = 0; i < options.size(); i++) {
				if (sData.trim().equals(options.get(i).getText().trim())) {
					matchFound = true;
				}
			}
			if (matchFound != true) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, sData + " is absent as list option");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sData + " is present as list option");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to store the inner text of webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void storeValue(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will temporary store text
		 * of the element
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			sStoredValue = element.getText();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sStoredValue,
					"Value stored is:: " + sStoredValue);
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used for verifying the presence of any text within the text stored by StoreValue keyword.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyStoredValue(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sTestScenario, String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the stored
		 * text to the text of the current element
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			if (element.getText().contains(sStoredValue)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "Stored Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, element.getText(),
						"Expected value is: " + sStoredValue + " 	Actual value is: " + element.getText());
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "color/font-family/font-size", child = "ObjectName", data = "Expected Value of Creative Attribute", description = "This keyword is used for verifying the creative atribute of the web element. PageName can be color, forn-family or font-size.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyCreativeAttribute(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the CCS
		 * attributes of the element one at a time
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			if (element.isEnabled()) {
				String sAttributeValue = element.getCssValue(sPage).toString();
				// flash(driver, element);
				iFlag = 1;
				if (sPage.contains("color")) {
					sAttributeValue = Color.fromString(sAttributeValue).asHex();
				} else if (sPage.equals("font-family")) {
					sAttributeValue = sAttributeValue.replaceAll("\\,.*", "");
					sAttributeValue = sAttributeValue.replaceAll("\"", "");
					sAttributeValue = sAttributeValue.replaceAll("\'", "");
				} else if (sPage.equals("font-size")) {
					String b;
					if (sAttributeValue.contains(".")) {
						b = sAttributeValue.substring(0, (sAttributeValue.length() - 5));
						int ifontsize = Integer.parseInt(b);
						int jfontsize = ifontsize + 1;
						sAttributeValue = Integer.toString(jfontsize) + "px";
					}
				}
				if (sData.equalsIgnoreCase(sAttributeValue)) {
					iTotalStepsPass = iTotalStepsPass + 1;
					rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
				} else {
					iTotalStepsFail = iTotalStepsFail + 1;
					// flashError(driver, element, sStep, sTestScenario,
					// deviceName);
					rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
							"Expected value is:: " + sData + " 	Actual value is:: " + sAttributeValue);
					iSummaryFlag = 1;
					/*
					 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
					 * iTCLoop = iTCRowCount; }
					 */
				}
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not enabled");
				iSummaryFlag = 1;
			}
		} catch (Exception e) {
			if (!(iFlag == 1)) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "expectedheight", description = "This keyword is used for verifying of the webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyHeight(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the Height of
		 * the element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			int iHeight = element.getSize().height;
			String sHeight = Integer.toString(iHeight);
			// flash(driver, element);
			iFlag = 1;
			if (sData.equalsIgnoreCase(sHeight)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Height is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected Height is:: " + sData + " 	Actual Height is:: " + sHeight);
				iSummaryFlag = 1;
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "ExpectedWidth", description = "This keyword is used for verifying width of the webelement.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyWidth(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the width of
		 * the element
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			int iWidth = element.getSize().width;
			String sWidth = Integer.toString(iWidth);
			// flash(driver, element);
			iFlag = 1;
			if (sData.equalsIgnoreCase(sWidth)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Width is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected Width is:: " + sData + " 	Actual Width is:: " + sWidth);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "ExpectedLeftMargin", description = "This keyword is used for verifying the left margin of the webelement. There is 5 pixels parity.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyLeftMargin(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the element
		 * position from the left side of the screen
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			int iLeftMargin = element.getLocation().x;
			int iLeftData = Integer.parseInt(sData);
			// flash(driver, element);
			iFlag = 1;
			if (iLeftMargin <= (iLeftData + 5) && iLeftMargin >= (iLeftData - 5)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Left Margin is within given tolerance level(5px) i.e. " + iLeftMargin);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected Left Margin is:: " + sData + " 	Actual Left Margin is:: " + iLeftMargin);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to activate and bring the webelement into focus.", projectName = "", type = KeywordType.WEB_ONLY)
	public void activate(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			JavascriptExecutor javascript = (JavascriptExecutor) driver;
			javascript.executeScript("arguments[0].focus()", element);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + ": webelement is clicked");
			if (deviceName.contains("Tab")) {
				Thread.sleep(10000);
			}
			if (deviceName.contains("Phone")) {
				Thread.sleep(10000);
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", sObject + " successfully Activated.");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..",
						"Exception occured in activating object - " + sObject);
				iSummaryFlag = 1;
				System.out.println("Exception occured in activating object - " + sObject);
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "ExpetedTopMargin", description = "This keyword is used for verifying the top margin of webelement. There is 5 pixels parity.", projectName = "", type = KeywordType.WEB_ONLY)
	public void verifyTopMargin(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will verify the element
		 * position from the top of the webpage
		 */
		try {
			iFlag = 0;
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			int iTopMargin = element.getLocation().y;
			int iTopData = Integer.parseInt(sData);
			// flash(driver, element);
			iFlag = 1;
			if (iTopMargin <= (iTopData + 5) && iTopMargin >= (iTopData - 5)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Top Margin is within given tolerance level(5px) i.e." + iTopMargin);
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected Top Margin is:: " + sData + " 	Actual Top Margin is:: " + iTopMargin);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			if (iFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Exception occurred.");
				iSummaryFlag = 1;
				System.out.println("Exception occurred.");
				e.printStackTrace();
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "text", description = "This keyword is used for verifying text in home story Component .", projectName = "Unilever D2.0", type = KeywordType.WEB_ONLY)
	public void verifyHomeStoryText(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			String attributeValue = element.getText().split("\n")[0];

			if (attributeValue.equalsIgnoreCase(sData)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "text", description = "This keyword is used for verifying widget id in olapic .", projectName = "Unilever D2.0", type = KeywordType.WEB_ONLY)
	public void verifyOlapicWidgetID(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			String attributeValue = (String) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].getAttribute('data-instance')", element);

			if (attributeValue.equalsIgnoreCase(sData)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Value is as expected");
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				// flashError(driver, element, sStep, sTestScenario,
				// deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
						"Expected value is:: " + sData + " 	Actual value is:: " + sValue);
				iSummaryFlag = 1;
				/*
				 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) {
				 * iTCLoop = iTCRowCount; }
				 */
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
			/*
			 * if (arrOnError.get(iTCLoop).equalsIgnoreCase("Stop")) { iTCLoop =
			 * iTCRowCount; }
			 */
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is only specific to project NHSBT. Iframe is handled internally in the keyword. It reads the url and launches in the bowser.", projectName = "NHSBT", type = KeywordType.WEB_ONLY)

	public void emailValidation(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
		 * 11-August-2014 Description : This function will perform Click
		 * operation on the element
		 */
		try {
			System.out.println("Inside Email Validation Function");
			iClickFlag = 0;
			driver.switchTo().frame("ifmail");

			wait = new WebDriverWait(driver, 10);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			if (element.isEnabled()) {
				// flash(driver, element);
				iClickFlag = 1;
				// Thread.sleep(5000);
				String sText = element.getText();
				System.out.println("Validation URL:: " + sText);
				driver.get(sText);

				iTotalStepsPass = iTotalStepsPass + 1;
				rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, "..", "Email validation is done");
				if (deviceName.contains("Tab")) {
					Thread.sleep(10000);
				}
				if (deviceName.contains("Phone")) {
					Thread.sleep(10000);
				}
			} else {
				iTotalStepsFail = iTotalStepsFail + 1;
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Element is disabled");
				iSummaryFlag = 1;
			}

		} catch (Exception e) {
			if (iClickFlag == 0) {
				iTotalStepsFail = iTotalStepsFail + 1;
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
				rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, "..", "Email validation failed");
				iSummaryFlag = 1;
			}
		}
	}

	@KeywordInfo(parent = "PageName", child = "ObjectName", data = "", description = "This keyword is used to double click", type = KeywordType.WEB_ONLY)

	public void doubleClick(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.elementToBeClickable(byProperty));
			// flash(driver, element);
			new Actions(driver).doubleClick(element).build().perform();
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "element is double clicked");
			Thread.sleep(500);
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "Java Script to be executed", description = "This keyword is used to execute java script", type = KeywordType.WEB_ONLY)
	public void executeJavaScript(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			((JavascriptExecutor) driver).executeScript(sData);
			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "JS has been executed");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to focus a web element except form' element", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void focusElement(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "element is focused");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to count search list.", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void doveSearchResultCount(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			List<WebElement> list = HelperUtils.loadAllElements(driver);
			if (Integer.parseInt(sData.trim()) == list.size())
				res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "count is same");
			else {
				throw new Exception("count not matched. Actual Count: " + list.size());
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword verify if search results are sorted. based on rating then their names", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void verifySearchSorting(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			List<WebElement> list = HelperUtils.loadAllElements(driver);
			List<SearchResult> s = new ArrayList<>();
			for (WebElement e : list) {
				SearchResult r = new SearchResult();
				r.setElement(e.getAttribute("outerHTML"));
				s.add(r);
			}
			Collections.sort(s, new SearchResultComparator());
			if (s.get(s.size() - 1).getName()
					.equals(list.get(0).findElement(By.cssSelector("h2.o-text__heading-3")).getText()))
				res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Sorting is correct");
			else {
				throw new Exception("Sorting is incorrect");
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "Tag's full text", description = "This keyword is used to insert tag", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void insertTag(WebDriver driver, By byProperty, Reports rep, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			// flash(driver, element);
			List<WebElement> elements = element.findElements(By.xpath(
					".//li[contains(@class,'coral-SelectList-item--option') and not(contains(@class,'is-hidden'))]"));
			int size = elements.size();
			WebElement el = elements.get(0);
			boolean node = false;
			int i = 1;
			while (i++ <= size) {
				if (el.getText().equalsIgnoreCase(sData)) {
					node = true;
					el.click();
					Thread.sleep(1000);
					break;
				} else {
					try {
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
						element.sendKeys(Keys.ARROW_DOWN);
						Thread.sleep(500);
						el = element.findElement(By.xpath(".//li[contains(@class, 'is-highlighted')]"));
					} catch (Exception e) {
					}
				}
			}
			if (!node) {
				throw new Exception("No tag found with text: " + sData);
			}

			iTotalStepsPass = iTotalStepsPass + 1;
			rep.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "element is double clicked");
		} catch (Exception e) {
			System.out.println(e);
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			rep.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to wait until element is present on page", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void waitUntilElementisPresent(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byProperty));
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "element is invisible now");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to close current tab/window of the browser", type = KeywordType.WEB_ONLY, projectName = "D2")
	public void closeWindow(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			if (driver.getWindowHandles().size() > 1) {
				driver.close();
				driver.switchTo().defaultContent();
			}
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "window is closed");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {

			}
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, "Unable to close window");
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "json text to compare", description = "This keyword is used to match json of the component. Element will be script tag in component", type = KeywordType.WEB_ONLY, projectName = "Unilever D 2.0")
	public void matchJSON(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped, String sAction,
			String sPage, String sObject, String sError, String sData, String sTestScenario, String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 60);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			String jsonD = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText",
					element);
			jsonD = jsonD.toString().trim();
			JSONObject json = new JSONObject(jsonD);
			JSONObject jd = new JSONObject(
					FileUtils.readFileToString(new File(sData), Charset.defaultCharset()).toString().trim());
			json = removeRandomNo(json);
			jd = removeRandomNo(jd);
			JsonObject j = new JsonObject();
			j.addProperty("component", json.toString());
			JsonObject k = new JsonObject();
			k.addProperty("component", jd.toString());

			if (!j.equals(k)) {
				throw new Exception("JSON data is not same");
			}
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "json data matched");
		} catch (D2Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {
				System.out.println(e1);
			}
			System.out.println(e);
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
			iSummaryFlag = 1;
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {
				System.out.println(e1);
			}
			System.out.println(e);
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "levels to compare", description = "This keyword is used to verify breadcrumb levels", type = KeywordType.WEB_ONLY, projectName = "Unilever D 2.0")
	public void verifyBreadCrumbLevel(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {

		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 60);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			int size = element.findElements(By.cssSelector("li.c-breadcrumb__item")).size();
			int urlLength = driver.getCurrentUrl().toLowerCase().replaceAll("https://", "").replaceAll("http://", "")
					.split("/").length + 1;
			int data = Integer.parseInt(sData);
			if (size == (urlLength - data)) {
				iTotalStepsPass = iTotalStepsPass + 1;
				res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Bread Crumb Level matched");
			} else {
				throw new Exception("Bread Crumb Level not matched");
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {
				System.out.println(e1);
			}
			System.out.println(e);
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " not found");
			iSummaryFlag = 1;
		}
	}

	private JSONObject removeRandomNo(JSONObject json) throws D2Exception {
		try {
			String no = json.getJSONObject("data").getString("randomNumber");
			if (!no.matches("\\d+")) {
				throw new D2Exception("Error in random number in script tag. Value is blank or not a number." + no);
			}
			json.getJSONObject("data").remove("randomNumber");
			no = json.getJSONObject("data").getJSONObject(json.getJSONObject("data").getString("componentType"))
					.getString("randomNumber");
			if (!no.matches("\\d+")) {
				throw new D2Exception("Error in random number in script tag. Value is blank or not a number." + no);
			}
			json.getJSONObject("data").getJSONObject(json.getJSONObject("data").getString("componentType"))
					.remove("randomNumber");
		} catch (JSONException e) {
			throw new D2Exception("Random Number not found in script tag");
		}
		return json;
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to verify any broken component on page", type = KeywordType.WEB_ONLY, projectName = "Unilever D 2.0")
	public void verifyBrokenComponent(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 18-October-2016
		 * 
		 */
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			List<WebElement> elements = driver.findElements(byProperty);
			if (!elements.isEmpty()) {
				String error = "Broken Component Path: ";
				for (WebElement ele : elements) {
					error += ele.findElement(By.xpath("./../cq")).getAttribute("data-path");
					error += "\nBroken Component Path: ";
				}
				throw new Exception(error);
			}
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "No Broken Components");
		} catch (TimeoutException ex) {
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "No Broken Components");
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {
				System.out.println(e1);
			}
			System.out.println(e);
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, e.getMessage());
			iSummaryFlag = 1;
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "..", description = "This keyword is used to verify if element has blank text", type = KeywordType.WEB_ONLY, projectName = "Unilever D 2.0")
	public void hasNonEmptyText(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 15-January-2017
		 * 
		 */
		try {
			wait = new WebDriverWait(driver, 60);
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));

			if (!element.getText().isEmpty()) {
				iTotalStepsPass = iTotalStepsPass + 1;
				res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "Element has non empty text");
			} else {
				throw new Exception("Element has empty text");
			}
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			try {
				TakeScreenshotTest(driver, sStep, sTestScenario, deviceName);
			} catch (Exception e1) {
				System.out.println(e1);
			}
			System.out.println(e);
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData, sObject + " " + e);
			iSummaryFlag = 1;
		}
	}

	@KeywordInfo(parent = "PageName", child = "..", data = "Folder/directory path to store image. It could be relative url w.r.t UTAS jar file or absolute directory path", description = "This keyword is used capture current page as Image", type = KeywordType.WEB_ONLY, projectName = "Unilever D 2.0")
	public void takePageScreenshot(WebDriver driver, By byProperty, Reports res, String sStep, String sTCMapped,
			String sAction, String sPage, String sObject, String sError, String sData, String sTestScenario,
			String deviceName) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Last Modified:
		 * 3-Feb-2017
		 * 
		 */
		try {

			// wait till page load
			boolean flag = true;
			do {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				String value = (String) executor.executeScript("return document.readyState");
				if (value.equalsIgnoreCase("complete")) {
					flag = false;
				}
				Thread.sleep(2000);
			} while (flag);
			// wating for page load block ended
			wait = new WebDriverWait(driver, 60);
			new File(sData).mkdirs();
			String path = TakeScreenshotTest(driver, sStep, sTestScenario, deviceName, sData);
			iTotalStepsPass = iTotalStepsPass + 1;
			res.writeTCPass(sStep, sTCMapped, sAction, sPage, sObject, sData, "WebPage captured at: " + path);
		} catch (Exception e) {
			iTotalStepsFail = iTotalStepsFail + 1;
			res.writeTCFail(sStep, sTCMapped, sAction, sPage, sObject, sData,
					sObject + " Not able to capture page. " + e);
			iSummaryFlag = 1;
		}

	}

	private String TakeScreenshotTest(WebDriver driver, String sStep, String sTestScenario, String deviceName,
			String path) {
		/*
		 * Author : Sachin Kumar (skumar213@sapient.com) Description : This
		 * function will take the Screen Shot of the current page view
		 */

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String filename = sTestScenario + "_" + deviceName + "_" + "Screenshot-" + sStep + ".png";
		File dest = new File(path, filename);
		try {
			FileUtils.copyFile(scrFile, new File(path, filename));
			return dest.getAbsolutePath();
		} catch (Exception e) {
			System.out.println("encountered somme error while capturing screenshot" + e);
		}
		return null;
	}

}
