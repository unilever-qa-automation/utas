package com.sapient.utas.objectrepository;

public class TestObject {
	private String locator = null;
	private String locatorValue = null;
	private String pageName = null;
	private String objectName = null;
	private String index = null;

	public TestObject(String pageName, String objectName, String locator, String locatorValue, String index) {
		super();
		this.pageName = pageName;
		this.objectName = objectName;
		this.locator = locator;
		this.locatorValue = locatorValue;
		this.index = index;
	}

	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}

	/**
	 * @return the objectName
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * @param pageName
	 *            the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * @param objectName
	 *            the objectName to set
	 */
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public TestObject() {
	}

	/**
	 * @return the locator
	 */
	public String getLocator() {
		return locator;
	}

	/**
	 * @return the locatorValue
	 */
	public String getLocatorValue() {
		return locatorValue;
	}

	/**
	 * @return the index
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * @param locator
	 *            the locator to set
	 */
	public void setLocator(String locator) {
		this.locator = locator;
	}

	/**
	 * @param locatorValue
	 *            the locatorValue to set
	 */
	public void setLocatorValue(String locatorValue) {
		this.locatorValue = locatorValue;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(String index) {
		this.index = index;
	}

}
