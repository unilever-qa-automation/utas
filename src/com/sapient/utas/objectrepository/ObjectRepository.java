package com.sapient.utas.objectrepository;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sapient.utas.config.UTASORConstants;

public class ObjectRepository {
	String strORFile = null;
	Document doc = null;

	public ObjectRepository(String strORFile) throws FileNotFoundException {
		this.strORFile = strORFile;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			if (new File(this.strORFile).exists()) {
				DocumentBuilder db = dbf.newDocumentBuilder();
				doc = db.parse(strORFile);
			} else {
				throw new FileNotFoundException(strORFile + " file is not found.");
			}

		} catch (Exception e) {
			this.strORFile = null;
			this.doc = null;
		}
	}

	public TestObject getTestObject(String strParent, String strChild) {
		try {
			String strXPath = "";
			if (strChild == null || strChild == "" || strChild.equals("..")) {
				return null;
			}
			if (strParent.trim().equalsIgnoreCase(strChild.trim())) {
				strXPath = "//" + UTASORConstants.OR_PARENT_NODENAME + "[@name='" + strChild + "']";
			} else if (!strParent.trim().equalsIgnoreCase(strChild.trim())) {
				strXPath = "//" + UTASORConstants.OR_PARENT_NODENAME + "[@name='" + strParent.trim() + "']//"
						+ UTASORConstants.OR_CHILD_NODENAME + "[@name='" + strChild.trim() + "']";
			} else {
				strXPath = "//child[@name='" + strChild + "']";
			}
			Node node = getNode(this, strXPath);
			String locator = ((Element) node).getElementsByTagName(UTASORConstants.OR_PROPERTY_NAME).item(0)
					.getTextContent();
			String locatorValue = ((Element) node).getElementsByTagName(UTASORConstants.OR_PROPERTY_VALUE).item(0)
					.getTextContent();
			String index = ((Element) node).getElementsByTagName(UTASORConstants.OR_INDEX).item(0).getTextContent();
			return new TestObject(strParent, strChild, locator, locatorValue, index);
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	private Node getNode(ObjectRepository or, String strXPath) {
		Node node = null;
		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			node = (Node) xpath.evaluate(strXPath, or.doc, XPathConstants.NODE);
			if (node != null) {
				return node;
			}

		} catch (Exception ex) {

		}
		return null;
	}

	// private String getParentNodeCaseInsensitiveXpathString(String
	// strParentName) {
	// return
	// "//parent[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')
	// = translate('"
	// + strParentName +
	// "','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')]";
	// }

	// private String getChildNodeCaseInsensitiveXpathString(String
	// strParentName, String strChildName) {
	// return getParentNodeCaseInsensitiveXpathString(strParentName)
	// +
	// "/child[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')
	// = translate('"
	// + strChildName +
	// "','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')]";
	// }
	//
	// private String getNodeCaseInsensitiveXpathString(String strNodeName,
	// String strAttrNameValue) {
	// return "//" + strNodeName
	// +
	// "[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')
	// = translate('"
	// + strAttrNameValue +
	// "','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')]";
	// }

}
