package com.sapient.utas.config;

public class UTASORConstants {

	public final static String OR_ROOT_NODENAME = "UTAS";
	public final static String OR_PARENT_NODENAME = "parent";
	public final static String OR_CHILD_NODENAME = "child";
	public final static String OR_PROPERTY_NAME = "propertyname";
	public final static String OR_PROPERTY_VALUE = "propertyvalue";
	public final static String OR_INDEX = "index";
	public final static String OR_NODE_ATTRIBUTE = "name";

	public final static String TREE_ROOT_NODE = "Test Objects";
	public final static String UTAS_OR_MANAGER_TITLE = "UTAS OR Manager";

	public final static String[] LOCATORs = new String[] { "title", "id", "name", "xpath", "linkText",
			"partialLinkText", "cssSelector", "tagName", "className" };
}
