package com.sapient.utas.webdriverutil;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import com.sapient.utas.objectrepository.TestObject;

public class FindBy extends By {

	@Override
	public List<WebElement> findElements(SearchContext arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public void getLocator(TestObject testObject) {

		By byProperty = null;
		try {
			/*
			 * Author : Rajesh Kumar (rkumar68@sapient.com) Last Modified:
			 * 11-August-2014 Description : This function will decide the
			 * locator to be used for element identification
			 */

			String sLocator = testObject.getLocator();
			String sLocator_Property = testObject.getLocatorValue();

			// Find the Element Property to be used for Identifying the Object
			if (sLocator.equalsIgnoreCase("css") || sLocator.equalsIgnoreCase("cssSelector")) {
				byProperty = By.cssSelector(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("id")) {
				byProperty = By.id(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("linkText")) {
				byProperty = By.linkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("name")) {
				byProperty = By.name(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("partialLinkText")) {
				byProperty = By.partialLinkText(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("tagName")) {
				byProperty = By.tagName(sLocator_Property);
			} else if (sLocator.equalsIgnoreCase("xpath")) {
				byProperty = By.xpath(sLocator_Property);
			}
		} catch (Exception e) {
			System.out.println("Ooooopppppppssss!! Error in GetLocator function");
			System.out.println(e);
		}
	}

}
