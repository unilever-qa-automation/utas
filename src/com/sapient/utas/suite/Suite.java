package com.sapient.utas.suite;

import java.util.ArrayList;
import java.util.Hashtable;

public class Suite {
	private String suiteName;
	private String suiteLevelGlobalTestDataFile;
	private String suiteLevelScriptFolder;
	private ArrayList<Hashtable<String, String>> suiteGlobalData;

	// private ArrayList<String> scenarios;

	public Suite(String suiteName, String suiteLevelGlobalTestDataFile, String suiteLevelScriptFolder) {
		this.suiteName = suiteName;
		this.suiteLevelGlobalTestDataFile = suiteLevelGlobalTestDataFile;
		this.suiteLevelScriptFolder = suiteLevelScriptFolder;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public String getSuiteLevelGlobalTestDataFile() {
		return suiteLevelGlobalTestDataFile;
	}

	public void setSuiteLevelGlobalTestDataFile(String suiteLevelGlobalTestDataFile) {
		this.suiteLevelGlobalTestDataFile = suiteLevelGlobalTestDataFile;
	}

	public String getSuiteLevelScriptFolder() {
		return suiteLevelScriptFolder;
	}

	public void setSuiteLevelScriptFolder(String suiteLevelScriptFolder) {
		this.suiteLevelScriptFolder = suiteLevelScriptFolder;
	}

	/**
	 * @return the suiteGlobalData
	 */
	public ArrayList<Hashtable<String, String>> getSuiteGlobalData() {
		return suiteGlobalData;
	}

	/**
	 * @param suiteGlobalData
	 *            the suiteGlobalData to set
	 */
	public void setSuiteGlobalData(ArrayList<Hashtable<String, String>> suiteGlobalData) {
		this.suiteGlobalData = suiteGlobalData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((suiteName == null) ? 0 : suiteName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Suite other = (Suite) obj;
		if (suiteName == null) {
			if (other.suiteName != null)
				return false;
		} else if (!suiteName.equals(other.suiteName))
			return false;
		return true;
	}

}
