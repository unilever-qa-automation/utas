@echo off

set newline=^& echo.

echo %newline%%newline%Automation Script Execution started using QASDG UNIFIED AUTOMATION SOLUTION Utility !! %newline%%newline%

echo Note:- PLEASE WAIT YOUR SCRIPT EXECUTION WILL START VERY SHORTLY %newline% 

java -Dwebdriver.gecko.driver=".\WebDriverDrivers\geckodriver.exe" -jar ".\utas_lib\selenium-server-standalone.jar" -role node -hub http://localhost:4444/grid/register -port 5555 -browser browserName=firefox %var%

SET /P Var= "EXECUTION END..Press Enter to Exit"
